package net.sssubtlety.sturdy_vehicles.datagen.creator;

import net.sssubtlety.sturdy_vehicles.LootKeys;
import net.sssubtlety.sturdy_vehicles.SturdyVehicles.PackIds;
import net.sssubtlety.sturdy_vehicles.datagen.creator.abstraction.BuiltinRegistryDependentPackCreator;
import net.sssubtlety.sturdy_vehicles.datagen.loot.SimpleLootTableProvider;

import com.google.common.collect.ImmutableMap;

import net.fabricmc.fabric.api.datagen.v1.FabricDataGenerator;
import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;
import net.fabricmc.fabric.api.datagen.v1.provider.SimpleFabricLootTableProvider;

import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.context.LootContextTypes;
import net.minecraft.registry.HolderLookup;
import net.minecraft.registry.RegistryKey;
import net.minecraft.util.Identifier;

import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public final class FragileOldBoatsCreator extends BuiltinRegistryDependentPackCreator {
    private static final ImmutableMap<Item, BoatLootKeys> LOOT_KEYS_BY_MATERIAL =
        ImmutableMap.of(
            Items.ACACIA_PLANKS, new BoatLootKeys(LootKeys.ACACIA_BOAT, LootKeys.ACACIA_CHEST_BOAT),
            Items.BAMBOO_PLANKS, new BoatLootKeys(LootKeys.BAMBOO_RAFT, LootKeys.BAMBOO_CHEST_RAFT),
            Items.BIRCH_PLANKS, new BoatLootKeys(LootKeys.BIRCH_BOAT, LootKeys.BIRCH_CHEST_BOAT),
            Items.CHERRY_PLANKS, new BoatLootKeys(LootKeys.CHERRY_BOAT, LootKeys.CHERRY_CHEST_BOAT),
            Items.DARK_OAK_PLANKS, new BoatLootKeys(LootKeys.DARK_OAK_BOAT, LootKeys.DARK_OAK_CHEST_BOAT),
            Items.JUNGLE_PLANKS, new BoatLootKeys(LootKeys.JUNGLE_BOAT, LootKeys.JUNGLE_CHEST_BOAT),
            Items.MANGROVE_PLANKS, new BoatLootKeys(LootKeys.MANGROVE_BOAT, LootKeys.MANGROVE_CHEST_BOAT),
            Items.OAK_PLANKS, new BoatLootKeys(LootKeys.OAK_BOAT, LootKeys.OAK_CHEST_BOAT),
            Items.PALE_OAK_PLANKS, new BoatLootKeys(LootKeys.PALE_OAK_BOAT, LootKeys.PALE_OAK_CHEST_BOAT),
            Items.SPRUCE_PLANKS, new BoatLootKeys(LootKeys.SPRUCE_BOAT, LootKeys.SPRUCE_CHEST_BOAT)
        );

    @Override
    protected Identifier getPackId() {
        return PackIds.FRAGILE_OLD_BOATS;
    }

    @Override
    protected Stream<FabricDataGenerator.Pack.RegistryDependentFactory<?>> streamRegistryDependentFactories() {
        return Stream.of(LootTableProvider::new);
    }

    private static class LootTableProvider extends SimpleFabricLootTableProvider {
        public LootTableProvider(
            FabricDataOutput output,
            CompletableFuture<HolderLookup.Provider> registryLookup
        ) {
            super(output, registryLookup, LootContextTypes.ENTITY);
        }

        @Override
        public void generate(BiConsumer<RegistryKey<LootTable>, LootTable.Builder> lootTableAdder) {
            LOOT_KEYS_BY_MATERIAL.forEach((material, keys) -> {
                lootTableAdder.accept(keys.boatKey, this.buildBoatLootTable(material));
                lootTableAdder.accept(keys.chestBoatKey, this.buildChestBoatLootTable(material));
            });
        }

        private SimpleLootTableProvider buildBoatLootTable(Item material) {
            return new SimpleLootTableProvider()
                .poolOf(material, 3)
                .poolOf(Items.STICK, 2);
        }

        private SimpleLootTableProvider buildChestBoatLootTable(Item material) {
            return this.buildBoatLootTable(material)
                .poolOf(Items.CHEST);
        }
    }

    private record BoatLootKeys(
        RegistryKey<LootTable> boatKey,
        RegistryKey<LootTable> chestBoatKey
    ) { }
}
