package net.sssubtlety.sturdy_vehicles.datagen.creator.abstraction;

import net.sssubtlety.sturdy_vehicles.Init.ContentAccess;

public abstract class BuiltinContentAccessingPackCreator extends BuiltinRegistryDependentPackCreator {
    protected final ContentAccess access;

    public BuiltinContentAccessingPackCreator(ContentAccess access) {
        this.access = access;
    }

    // protected abstract Stream<? extends ContentAccessingRecipesProvider.DataProvider.Factory<?, ?>>
    //     streamContentAccessingDataProviders();
    //
    // @Override
    // protected Stream<Pack.RegistryDependentFactory<?>> streamRegistryDependentFactories() {
    //     return this.streamContentAccessingDataProviders()
    //         .map(dataProvider -> dataProvider.toRegistryDependentFactory(this.access));
    // }
}
