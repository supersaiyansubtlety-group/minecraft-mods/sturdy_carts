package net.sssubtlety.sturdy_vehicles.datagen.creator.abstraction;

import net.fabricmc.fabric.api.datagen.v1.FabricDataGenerator;
import net.fabricmc.fabric.api.datagen.v1.FabricDataGenerator.Pack;
import net.fabricmc.fabric.api.datagen.v1.FabricDataGenerator.Pack.RegistryDependentFactory;

import java.util.stream.Stream;

public abstract class BuiltinRegistryDependentPackCreator extends BuiltinPackCreator {
    protected abstract Stream<RegistryDependentFactory<?>> streamRegistryDependentFactories();

    @Override
    public Pack createImpl(FabricDataGenerator generator) {
        final Pack pack = super.createImpl(generator);

        this.streamRegistryDependentFactories().forEach(pack::addProvider);

        return pack;
    }
}
