package net.sssubtlety.sturdy_vehicles.datagen.creator.abstraction;

import net.sssubtlety.sturdy_vehicles.Init;

import net.minecraft.recipe.RecipeCategory;

public abstract class BuiltinTransportationRecipeFilteringPackCreator extends BuiltinRecipeFilteringPackCreator {
    public BuiltinTransportationRecipeFilteringPackCreator(Init.ContentAccess access) {
        super(access);
    }

    @Override
    protected final RecipeCategory getRecipeCategory() {
        return RecipeCategory.TRANSPORTATION;
    }
}
