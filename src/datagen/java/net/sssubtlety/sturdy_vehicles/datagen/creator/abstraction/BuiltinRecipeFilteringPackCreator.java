package net.sssubtlety.sturdy_vehicles.datagen.creator.abstraction;

import net.sssubtlety.sturdy_vehicles.Init;
import net.sssubtlety.sturdy_vehicles.datagen.util.DataGenUtil;

import net.fabricmc.fabric.api.datagen.v1.FabricDataGenerator.Pack;

import net.minecraft.data.PackMetadataProvider;
import net.minecraft.recipe.RecipeCategory;
import net.minecraft.resource.pack.metadata.ResourceFilterMetadataSection;
import net.minecraft.util.Identifier;

import java.util.stream.Stream;

import static net.sssubtlety.sturdy_vehicles.datagen.util.DataGenUtil.exactRecipeAdvancementPatterOf;

public abstract class BuiltinRecipeFilteringPackCreator extends BuiltinContentAccessingPackCreator {
    public BuiltinRecipeFilteringPackCreator(Init.ContentAccess access) {
        super(access);
    }

    protected abstract Stream<Identifier> streamFilteredRecipeIds();

    protected abstract RecipeCategory getRecipeCategory();

    @Override
    protected Pack.Factory<PackMetadataProvider> createPackMetadataFactory() {
        return output -> {
            final PackMetadataProvider metadata = super.createPackMetadataFactory().create(output);

            metadata.add(ResourceFilterMetadataSection.TYPE, new ResourceFilterMetadataSection(
                Stream.concat(
                    this.streamFilteredRecipeIds().map(DataGenUtil::exactRecipePatternOf),
                    this.streamFilteredRecipeIds()
                        .map(recipeId -> exactRecipeAdvancementPatterOf(recipeId, this.getRecipeCategory()))

                ).toList()
            ));

            return metadata;
        };
    }
}
