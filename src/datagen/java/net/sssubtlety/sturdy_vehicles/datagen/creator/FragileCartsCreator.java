package net.sssubtlety.sturdy_vehicles.datagen.creator;

import net.sssubtlety.sturdy_vehicles.LootKeys;
import net.sssubtlety.sturdy_vehicles.SturdyVehicles;
import net.sssubtlety.sturdy_vehicles.datagen.creator.abstraction.BuiltinRegistryDependentPackCreator;
import net.sssubtlety.sturdy_vehicles.datagen.loot.SimpleLootTableProvider;

import net.fabricmc.fabric.api.datagen.v1.FabricDataGenerator.Pack;
import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;
import net.fabricmc.fabric.api.datagen.v1.provider.SimpleFabricLootTableProvider;

import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.context.LootContextTypes;
import net.minecraft.registry.HolderLookup;
import net.minecraft.registry.RegistryKey;
import net.minecraft.util.Identifier;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public final class FragileCartsCreator extends BuiltinRegistryDependentPackCreator {
    private static final Map<RegistryKey<LootTable>, Item> ADDITIONAL_DROPS_BY_KEY = Map.of(
        LootKeys.CHEST_MINECART, Items.CHEST,
        LootKeys.FURNACE_MINECART, Items.FURNACE,
        LootKeys.HOPPER_MINECART, Items.HOPPER,
        LootKeys.TNT_MINECART, Items.TNT
    );

    @Override
    protected Identifier getPackId() {
        return SturdyVehicles.PackIds.FRAGILE_CARTS;
    }

    @Override
    protected Stream<Pack.RegistryDependentFactory<?>> streamRegistryDependentFactories() {
        return Stream.of(LootTableProvider::new);
    }

    private static class LootTableProvider extends SimpleFabricLootTableProvider {
        public LootTableProvider(
            FabricDataOutput output,
            CompletableFuture<HolderLookup.Provider> registryLookup
        ) {
            super(output, registryLookup, LootContextTypes.ENTITY);
        }

        @Override
        public void generate(BiConsumer<RegistryKey<LootTable>, LootTable.Builder> biConsumer) {
            ADDITIONAL_DROPS_BY_KEY.forEach((key, additionalDrop) ->
                biConsumer.accept(key, this.buildLootTable(additionalDrop))
            );
        }

        private SimpleLootTableProvider buildLootTable(Item addition) {
            return new SimpleLootTableProvider()
                .poolOf(addition)
                .poolOf(Items.MINECART);
        }
    }
}
