package net.sssubtlety.sturdy_vehicles.datagen.creator;

import net.sssubtlety.sturdy_vehicles.LootKeys;
import net.sssubtlety.sturdy_vehicles.SturdyVehicles.PackIds;
import net.sssubtlety.sturdy_vehicles.datagen.creator.abstraction.BuiltinRegistryDependentPackCreator;
import net.sssubtlety.sturdy_vehicles.datagen.loot.SimpleLootTableProvider;

import net.fabricmc.fabric.api.datagen.v1.FabricDataGenerator;
import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;
import net.fabricmc.fabric.api.datagen.v1.provider.SimpleFabricLootTableProvider;

import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.context.LootContextTypes;
import net.minecraft.registry.HolderLookup;
import net.minecraft.registry.RegistryKey;
import net.minecraft.util.Identifier;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public final class FragileChestBoatsCreator extends BuiltinRegistryDependentPackCreator {
    private static final Map<RegistryKey<LootTable>, Item> BASE_DROPS_BY_KEY = Map.of(
        LootKeys.ACACIA_CHEST_BOAT, Items.ACACIA_BOAT,
        LootKeys.BAMBOO_CHEST_RAFT, Items.BAMBOO_RAFT,
        LootKeys.BIRCH_CHEST_BOAT, Items.BIRCH_BOAT,
        LootKeys.CHERRY_CHEST_BOAT, Items.CHERRY_BOAT,
        LootKeys.DARK_OAK_CHEST_BOAT, Items.DARK_OAK_BOAT,
        LootKeys.JUNGLE_CHEST_BOAT, Items.JUNGLE_BOAT,
        LootKeys.MANGROVE_CHEST_BOAT, Items.MANGROVE_BOAT,
        LootKeys.OAK_CHEST_BOAT, Items.OAK_BOAT,
        LootKeys.PALE_OAK_CHEST_BOAT, Items.PALE_OAK_BOAT,
        LootKeys.SPRUCE_CHEST_BOAT, Items.SPRUCE_BOAT
    );

    @Override
    protected Identifier getPackId() {
        return PackIds.FRAGILE_CHEST_BOATS;
    }

    @Override
    protected Stream<FabricDataGenerator.Pack.RegistryDependentFactory<?>> streamRegistryDependentFactories() {
        return Stream.of(LootTableProvider::new);
    }

    private static class LootTableProvider extends SimpleFabricLootTableProvider {
        public LootTableProvider(
            FabricDataOutput output,
            CompletableFuture<HolderLookup.Provider> registryLookup
        ) {
            super(output, registryLookup, LootContextTypes.ENTITY);
        }

        @Override
        public void generate(BiConsumer<RegistryKey<LootTable>, LootTable.Builder> biConsumer) {
            BASE_DROPS_BY_KEY.forEach((key, baseDrop) ->
                biConsumer.accept(key, this.buildLootTable(baseDrop))
            );
        }

        private SimpleLootTableProvider buildLootTable(Item item) {
            return new SimpleLootTableProvider()
                .poolOf(Items.CHEST)
                .poolOf(item);
        }
    }
}
