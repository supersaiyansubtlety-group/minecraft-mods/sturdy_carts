package net.sssubtlety.sturdy_vehicles.datagen.creator;

import net.sssubtlety.sturdy_vehicles.Init.ContentAccess;
import net.sssubtlety.sturdy_vehicles.SturdyVehicles.PackIds;
import net.sssubtlety.sturdy_vehicles.datagen.creator.abstraction.BuiltinTransportationRecipeFilteringPackCreator;
import net.sssubtlety.sturdy_vehicles.datagen.recipe.CombiningRecipesProvider;
import net.sssubtlety.sturdy_vehicles.datagen.recipe.IdRetainingRecipesDataProvider;
import net.sssubtlety.sturdy_vehicles.datagen.util.NamedCriterion;

import net.fabricmc.fabric.api.datagen.v1.FabricDataGenerator;
import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;

import net.minecraft.data.server.recipe.RecipeExporter;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.registry.HolderLookup;
import net.minecraft.registry.Registries;
import net.minecraft.util.Identifier;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public final class KeepMinecartNamesCreator extends BuiltinTransportationRecipeFilteringPackCreator {
    private static final Map<Item, Item> RESULTS_BY_ADDITION = Map.of(
        Items.CHEST, Items.CHEST_MINECART,
        Items.FURNACE, Items.FURNACE_MINECART,
        Items.HOPPER, Items.HOPPER_MINECART,
        Items.TNT, Items.TNT_MINECART
    );

    public KeepMinecartNamesCreator(ContentAccess access) {
        super(access);
    }

    @Override
    protected Identifier getPackId() {
        return PackIds.KEEP_MINECART_NAMES;
    }

    @Override
    protected Stream<FabricDataGenerator.Pack.RegistryDependentFactory<?>> streamRegistryDependentFactories() {
        return Stream.of(DataProvider::new);
    }

    @Override
    protected Stream<Identifier> streamFilteredRecipeIds() {
        return RESULTS_BY_ADDITION.values().stream().map(Registries.ITEM::getId);
    }

    private class RecipesProvider extends CombiningRecipesProvider {
        public RecipesProvider(
            HolderLookup.Provider registries, RecipeExporter exporter,
            ContentAccess access
        ) {
            super(registries, exporter, access);
        }

        @Override
        public void generateRecipes() {
            RESULTS_BY_ADDITION
                .forEach((addition, result) -> this.acceptCombiningRecipe(
                    this.exporter,
                    addition, Items.MINECART, result,
                    NamedCriterion.hasMinecart(this),
                    KeepMinecartNamesCreator.this.getRecipeCategory()
                ));
        }
    }

    private class DataProvider extends IdRetainingRecipesDataProvider {
        public DataProvider(FabricDataOutput output, CompletableFuture<HolderLookup.Provider> registriesFuture) {
            super(output, registriesFuture);
        }

        @Override
        protected RecipesProvider method_62766(HolderLookup.Provider registryLookup, RecipeExporter exporter) {
            return new RecipesProvider(registryLookup, exporter, KeepMinecartNamesCreator.this.access);
        }

        @Override
        public String getDescription() {
            return "Keep Minecart Names recipes";
        }
    }
}
