package net.sssubtlety.sturdy_vehicles.datagen.creator.abstraction;

import net.fabricmc.fabric.api.datagen.v1.FabricDataGenerator;
import net.fabricmc.fabric.api.datagen.v1.FabricDataGenerator.Pack;

import net.minecraft.SharedConstants;
import net.minecraft.data.PackMetadataProvider;
import net.minecraft.resource.ResourceType;
import net.minecraft.resource.pack.metadata.PackResourceMetadataSection;
import net.minecraft.util.Identifier;

import java.util.Optional;

import static net.sssubtlety.sturdy_vehicles.datagen.util.DataGenUtil.packDescriptionOf;

public abstract class BuiltinPackCreator {
    protected abstract Identifier getPackId();

    protected Pack.Factory<PackMetadataProvider> createPackMetadataFactory() {
        return output -> {
            final PackMetadataProvider packMetadata = new PackMetadataProvider(output);

            packMetadata.add(
                PackResourceMetadataSection.TYPE,
                new PackResourceMetadataSection(
                    packDescriptionOf(this.getPackId()),
                    SharedConstants.getGameVersion().getResourceVersion(ResourceType.SERVER_DATA),
                    Optional.empty()
                )
            );

            return packMetadata;
        };
    }

    public final void create(FabricDataGenerator generator) {
        this.createImpl(generator);
    }

    protected Pack createImpl(FabricDataGenerator generator) {
        final Pack pack = generator.createBuiltinResourcePack(this.getPackId());
        pack.addProvider(this.createPackMetadataFactory());
        return pack;
    }
}
