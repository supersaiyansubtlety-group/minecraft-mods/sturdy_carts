package net.sssubtlety.sturdy_vehicles.datagen.creator;

import net.sssubtlety.sturdy_vehicles.Init.ContentAccess;
import net.sssubtlety.sturdy_vehicles.SturdyVehicles.PackIds;
import net.sssubtlety.sturdy_vehicles.datagen.creator.abstraction.BuiltinTransportationRecipeFilteringPackCreator;
import net.sssubtlety.sturdy_vehicles.datagen.recipe.CombiningRecipesProvider;
import net.sssubtlety.sturdy_vehicles.datagen.recipe.IdRetainingRecipesDataProvider;
import net.sssubtlety.sturdy_vehicles.datagen.util.BoatPair;
import net.sssubtlety.sturdy_vehicles.datagen.util.NamedCriterion;

import net.fabricmc.fabric.api.datagen.v1.FabricDataGenerator;
import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;

import net.minecraft.data.server.recipe.RecipeExporter;
import net.minecraft.item.Items;
import net.minecraft.registry.HolderLookup;
import net.minecraft.registry.Registries;
import net.minecraft.util.Identifier;

import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public final class KeepChestBoatNamesCreator extends BuiltinTransportationRecipeFilteringPackCreator {
    public KeepChestBoatNamesCreator(ContentAccess access) {
        super(access);
    }

    @Override
    protected Identifier getPackId() {
        return PackIds.KEEP_CHEST_BOAT_NAMES;
    }

    @Override
    protected Stream<FabricDataGenerator.Pack.RegistryDependentFactory<?>> streamRegistryDependentFactories() {
        return Stream.of(DataProvider::new);
    }

    @Override
    protected Stream<Identifier> streamFilteredRecipeIds() {
        return BoatPair.PAIRS.stream().map(BoatPair::chestBoat).map(Registries.ITEM::getId);
    }

    private class RecipesProvider extends CombiningRecipesProvider {
        public RecipesProvider(
            HolderLookup.Provider registries, RecipeExporter exporter
        ) {
            super(registries, exporter, KeepChestBoatNamesCreator.this.access);
        }

        @Override
        public void generateRecipes() {
            BoatPair.PAIRS
                .forEach(pair -> this.acceptCombiningRecipe(
                    this.exporter,
                    Items.CHEST, pair.boat(), pair.chestBoat(),
                    NamedCriterion.hasBoat(this),
                    KeepChestBoatNamesCreator.this.getRecipeCategory()
                ));
        }
    }

    private class DataProvider extends IdRetainingRecipesDataProvider {
        public DataProvider(
            FabricDataOutput output, CompletableFuture<HolderLookup.Provider> registriesFuture
        ) {
            super(output, registriesFuture);
        }

        @Override
        protected RecipesProvider method_62766(
            HolderLookup.Provider registries, RecipeExporter exporter
        ) {
            return new RecipesProvider(registries, exporter);
        }

        @Override
        public String getDescription() {
            return "Keep Chest Boat Names recipes";
        }
    }
}
