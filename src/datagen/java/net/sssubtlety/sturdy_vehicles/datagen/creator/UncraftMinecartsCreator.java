package net.sssubtlety.sturdy_vehicles.datagen.creator;

import net.sssubtlety.sturdy_vehicles.Init.ContentAccess;
import net.sssubtlety.sturdy_vehicles.SturdyVehicles.PackIds;
import net.sssubtlety.sturdy_vehicles.datagen.creator.abstraction.BuiltinContentAccessingPackCreator;
import net.sssubtlety.sturdy_vehicles.datagen.recipe.CompositeVehicleUncraftingRecipesProvider;
import net.sssubtlety.sturdy_vehicles.datagen.recipe.IdRetainingRecipesDataProvider;
import net.sssubtlety.sturdy_vehicles.datagen.util.NamedCriterion;

import net.fabricmc.fabric.api.datagen.v1.FabricDataGenerator.Pack.RegistryDependentFactory;
import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;

import net.minecraft.data.server.recipe.RecipeExporter;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.registry.HolderLookup;
import net.minecraft.util.Identifier;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public final class UncraftMinecartsCreator extends BuiltinContentAccessingPackCreator {
    private static final Map<Item, Item> RESULTS_BY_COMPOSITE = Map.of(
        Items.CHEST_MINECART, Items.CHEST,
        Items.FURNACE_MINECART, Items.FURNACE,
        Items.HOPPER_MINECART, Items.HOPPER,
        Items.TNT_MINECART, Items.TNT
    );

    public UncraftMinecartsCreator(ContentAccess access) {
        super(access);
    }

    @Override
    protected Identifier getPackId() {
        return PackIds.UNCRAFT_MINECARTS;
    }

    @Override
    protected Stream<RegistryDependentFactory<?>> streamRegistryDependentFactories() {
        return Stream.of(DataProvider::new);
    }

    private class RecipesProvider extends CompositeVehicleUncraftingRecipesProvider {
        public RecipesProvider(HolderLookup.Provider registries, RecipeExporter exporter) {
            super(registries, exporter, UncraftMinecartsCreator.this.access);
        }

        @Override
        public void generateRecipes() {
            RESULTS_BY_COMPOSITE
                .forEach((composite, result) -> this.acceptUncraftingRecipe(
                    this.exporter,
                    composite, Items.MINECART, result,
                    NamedCriterion.hasMinecart(this)
                ));
        }
    }

    private class DataProvider extends IdRetainingRecipesDataProvider {
        public DataProvider(FabricDataOutput output, CompletableFuture<HolderLookup.Provider> registriesFuture) {
            super(output, registriesFuture);
        }

        @Override
        protected RecipesProvider method_62766(HolderLookup.Provider registries, RecipeExporter exporter) {
            return new RecipesProvider(registries, exporter);
        }

        @Override
        public String getDescription() {
            return "Uncraft Minecarts recipes";
        }
    }
}
