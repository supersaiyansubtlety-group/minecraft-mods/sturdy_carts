package net.sssubtlety.sturdy_vehicles.datagen.creator;

import net.sssubtlety.sturdy_vehicles.Init.ContentAccess;
import net.sssubtlety.sturdy_vehicles.SturdyVehicles.PackIds;
import net.sssubtlety.sturdy_vehicles.datagen.creator.abstraction.BuiltinContentAccessingPackCreator;
import net.sssubtlety.sturdy_vehicles.datagen.recipe.CompositeVehicleUncraftingRecipesProvider;
import net.sssubtlety.sturdy_vehicles.datagen.recipe.IdRetainingRecipesDataProvider;
import net.sssubtlety.sturdy_vehicles.datagen.util.BoatPair;
import net.sssubtlety.sturdy_vehicles.datagen.util.NamedCriterion;

import net.fabricmc.fabric.api.datagen.v1.FabricDataGenerator;
import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;

import net.minecraft.data.server.recipe.RecipeExporter;
import net.minecraft.item.Items;
import net.minecraft.registry.HolderLookup;
import net.minecraft.util.Identifier;

import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public final class UncraftChestBoatsCreator extends BuiltinContentAccessingPackCreator {
    public UncraftChestBoatsCreator(ContentAccess access) {
        super(access);
    }

    @Override
    protected Identifier getPackId() {
        return PackIds.UNCRAFT_CHEST_BOATS;
    }

    @Override
    protected Stream<FabricDataGenerator.Pack.RegistryDependentFactory<?>> streamRegistryDependentFactories() {
        return Stream.of(DataProvider::new);
    }

    private class RecipesProvider extends CompositeVehicleUncraftingRecipesProvider {
        public RecipesProvider(HolderLookup.Provider registries, RecipeExporter exporter) {
            super(registries, exporter, UncraftChestBoatsCreator.this.access);
        }

        @Override
        public void generateRecipes() {
            BoatPair.PAIRS.forEach(pair ->
                this.acceptUncraftingRecipe(
                    this.exporter,
                    pair.chestBoat(), pair.boat(), Items.CHEST,
                    NamedCriterion.hasBoat(this)
                )
            );
        }
    }

    private class DataProvider extends IdRetainingRecipesDataProvider {
        public DataProvider(FabricDataOutput output, CompletableFuture<HolderLookup.Provider> registriesFuture) {
            super(output, registriesFuture);
        }

        @Override
        protected RecipesProvider method_62766(HolderLookup.Provider registries, RecipeExporter exporter) {
            return new RecipesProvider(registries, exporter);
        }

        @Override
        public String getDescription() {
            return "Uncraft Chest Boats recipes";
        }
    }
}
