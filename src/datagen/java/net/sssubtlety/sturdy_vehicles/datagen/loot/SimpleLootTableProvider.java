package net.sssubtlety.sturdy_vehicles.datagen.loot;

import net.minecraft.item.Item;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.entry.ItemEntry;
import net.minecraft.loot.function.LootFunction;
import net.minecraft.loot.function.SetCountLootFunction;
import net.minecraft.loot.provider.number.ConstantLootNumberProvider;
import net.minecraft.util.Identifier;
import net.minecraft.util.context.ContextKeySet;

public class SimpleLootTableProvider extends LootTable.Builder {
    public SimpleLootTableProvider poolOf(Item item) {
        return this.pool(LootPool.builder().with(ItemEntry.builder(item)));
    }

    public SimpleLootTableProvider poolOf(Item item, int count) {
        return this.pool(
            LootPool.builder().with(
                ItemEntry.builder(item)
                    .apply(SetCountLootFunction.builder(ConstantLootNumberProvider.create(count)))
            )
        );
    }

    @Override
    public SimpleLootTableProvider pool(LootPool.Builder poolBuilder) {
        super.pool(poolBuilder);

        return this;
    }

    @Override
    public SimpleLootTableProvider type(ContextKeySet type) {
        super.type(type);

        return this;
    }

    @Override
    public SimpleLootTableProvider randomSequence(Identifier id) {
        super.randomSequence(id);

        return this;
    }

    @Override
    public SimpleLootTableProvider apply(LootFunction.Builder builder) {
        super.apply(builder);

        return this;
    }

    @Override
    public SimpleLootTableProvider unwrap() {
        super.unwrap();

        return this;
    }
}
