package net.sssubtlety.sturdy_vehicles.datagen.mixin.accessor;

import net.minecraft.util.IdentifierPattern;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

import java.util.Optional;
import java.util.regex.Pattern;

@Mixin(IdentifierPattern.class)
public interface IdentifierPatternAccessor {
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    @Invoker("<init>")
    static IdentifierPattern sturdy_carts_datagen$construct(
        Optional<Pattern> namespacePattern,
        Optional<Pattern> pathPattern
    ) {
        throw new IllegalStateException("Dummy method body invoked!");
    }
}
