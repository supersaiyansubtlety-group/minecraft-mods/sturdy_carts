package net.sssubtlety.sturdy_vehicles.datagen.util;

import net.minecraft.advancement.AdvancementCriterion;
import net.minecraft.data.server.RecipesProvider;
import net.minecraft.item.Items;
import net.minecraft.registry.tag.ItemTags;

public record NamedCriterion(String name, AdvancementCriterion<?> criterion) {
    public static NamedCriterion hasMinecart(RecipesProvider recipesProvider) {
        return new NamedCriterion(
            "has_minecart",
            recipesProvider.conditionsFromItem(Items.MINECART)
        );
    }

    public static NamedCriterion hasBoat(RecipesProvider recipesProvider) {
        return new NamedCriterion(
            "has_boat",
            recipesProvider.conditionsFromTag(ItemTags.BOATS)
        );
    }
}
