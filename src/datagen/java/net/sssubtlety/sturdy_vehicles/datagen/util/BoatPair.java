package net.sssubtlety.sturdy_vehicles.datagen.util;

import com.google.common.collect.ImmutableList;

import net.minecraft.item.Item;
import net.minecraft.item.Items;

public record BoatPair(Item boat, Item chestBoat) {
    public static final ImmutableList<BoatPair> PAIRS = ImmutableList.of(
        new BoatPair(Items.ACACIA_BOAT, Items.ACACIA_CHEST_BOAT),
        new BoatPair(Items.BAMBOO_RAFT, Items.BAMBOO_CHEST_RAFT),
        new BoatPair(Items.BIRCH_BOAT, Items.BIRCH_CHEST_BOAT),
        new BoatPair(Items.CHERRY_BOAT, Items.CHERRY_CHEST_BOAT),
        new BoatPair(Items.DARK_OAK_BOAT, Items.DARK_OAK_CHEST_BOAT),
        new BoatPair(Items.JUNGLE_BOAT, Items.JUNGLE_CHEST_BOAT),
        new BoatPair(Items.MANGROVE_BOAT, Items.MANGROVE_CHEST_BOAT),
        new BoatPair(Items.OAK_BOAT, Items.OAK_CHEST_BOAT),
        new BoatPair(Items.PALE_OAK_BOAT, Items.PALE_OAK_CHEST_BOAT),
        new BoatPair(Items.SPRUCE_BOAT, Items.SPRUCE_CHEST_BOAT)
    );
}
