package net.sssubtlety.sturdy_vehicles.datagen.util;

import net.sssubtlety.sturdy_vehicles.datagen.mixin.accessor.IdentifierPatternAccessor;

import net.minecraft.recipe.RecipeCategory;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.util.IdentifierPattern;

import java.util.Optional;
import java.util.regex.Pattern;

public final class DataGenUtil {
    private DataGenUtil() { }

    public static Text packDescriptionOf(Identifier recipeId) {
        return Text.translatable(
            String.join(".", "pack", recipeId.getNamespace(), recipeId.getPath(), "description")
        );
    }

    public static Identifier recipeAdvancementIdOf(Identifier recipeId, RecipeCategory category) {
        return recipeId.withPrefix("recipes/" + category.getName() + "/");
    }

    public static IdentifierPattern exactRecipePatternOf(Identifier recipeId) {
        return exactPatternOf(recipeId.withPrefix("recipe/"));
    }

    public static IdentifierPattern exactRecipeAdvancementPatterOf(Identifier recipeId, RecipeCategory category) {
        return exactPatternOf(recipeAdvancementIdOf(recipeId, category).withPrefix("advancement/"));
    }

    public static IdentifierPattern exactPatternOf(Identifier id) {
        return IdentifierPatternAccessor.sturdy_carts_datagen$construct(
            Optional.of(completeLiteralPatternOf(id.getNamespace())),
            Optional.of(completeLiteralPatternOf(id.getPath() + ".json"))
        );
    }

    public static Pattern completeLiteralPatternOf(String literal) {
        return Pattern.compile("^" + Pattern.quote(literal) + "$");
    }
}
