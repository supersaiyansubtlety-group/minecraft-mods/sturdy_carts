package net.sssubtlety.sturdy_vehicles.datagen.recipe;

import net.sssubtlety.sturdy_vehicles.Init;
import net.sssubtlety.sturdy_vehicles.datagen.util.NamedCriterion;

import net.minecraft.data.server.recipe.RecipeExporter;
import net.minecraft.item.Item;
import net.minecraft.recipe.RecipeCategory;
import net.minecraft.registry.HolderLookup;

public abstract class CompositeVehicleUncraftingRecipesProvider extends SplittingRecipesProvider {
    public CompositeVehicleUncraftingRecipesProvider(
        HolderLookup.Provider registries, RecipeExporter exporter,
        Init.ContentAccess access
    ) {
        super(registries, exporter, access);
    }

    protected void acceptUncraftingRecipe(
        RecipeExporter exporter,
        Item composite, Item base, Item result,
        NamedCriterion criterion
    ) {
        this.acceptSplittingRecipe(
            exporter,
            composite, base, result,
            criterion,
            "uncraft_", RecipeCategory.TRANSPORTATION
        );
    }
}
