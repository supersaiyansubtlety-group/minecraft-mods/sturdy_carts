package net.sssubtlety.sturdy_vehicles.datagen.recipe;

import net.sssubtlety.sturdy_vehicles.Init.ContentAccess;
import net.sssubtlety.sturdy_vehicles.SturdyVehicles;
import net.sssubtlety.sturdy_vehicles.datagen.util.DataGenUtil;
import net.sssubtlety.sturdy_vehicles.datagen.util.NamedCriterion;

import net.minecraft.advancement.Advancement;
import net.minecraft.advancement.AdvancementCriterion;
import net.minecraft.advancement.AdvancementRequirements;
import net.minecraft.advancement.AdvancementRewards;
import net.minecraft.data.server.recipe.RecipeExporter;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.CraftingCategory;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.RecipeCategory;
import net.minecraft.registry.HolderLookup;
import net.minecraft.registry.Registries;
import net.minecraft.registry.RegistryKey;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.util.Identifier;

public abstract class SplittingRecipesProvider extends ContentAccessingRecipesProvider {
    public SplittingRecipesProvider(
        HolderLookup.Provider registries, RecipeExporter exporter,
        ContentAccess access
    ) {
        super(registries, exporter, access);
    }

    protected void acceptSplittingRecipe(
        RecipeExporter exporter,
        Item composite, Item base, Item result,
        String criterionName, AdvancementCriterion<?> criterion,
        String idPrefix, RecipeCategory category
    ) {
        final Identifier id = SturdyVehicles.idOf(Registries.ITEM.getId(composite).withPrefix(idPrefix).getPath());
        final RegistryKey<Recipe<?>> key = RegistryKey.of(RegistryKeys.RECIPE, id);

        final Advancement.Builder advancement = exporter.accept()
            .putCriteria(criterionName, criterion)
            .rewards(AdvancementRewards.Builder.recipe(key))
            .merger(AdvancementRequirements.RequirementMerger.ANY);

        exporter.accept(
            key,
            this.access.splittingSerializer().recipeOf(
                Ingredient.ofItems(composite), base.getDefaultStack(),
                new ItemStack(result), CraftingCategory.MISC
            ),
            advancement.build(DataGenUtil.recipeAdvancementIdOf(id, category))
        );
    }

    @SuppressWarnings("SameParameterValue")
    protected void acceptSplittingRecipe(
        RecipeExporter exporter,
        Item composite, Item base, Item result,
        NamedCriterion criterion,
        String idPrefix, RecipeCategory category
    ) {
        this.acceptSplittingRecipe(
            exporter,
            composite, base, result,
            criterion.name(), criterion.criterion(),
            idPrefix, category
        );
    }
}
