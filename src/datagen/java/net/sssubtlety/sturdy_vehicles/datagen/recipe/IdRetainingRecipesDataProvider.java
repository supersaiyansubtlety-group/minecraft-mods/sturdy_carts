package net.sssubtlety.sturdy_vehicles.datagen.recipe;

import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;
import net.fabricmc.fabric.api.datagen.v1.provider.FabricRecipeProvider;
import net.minecraft.registry.HolderLookup;
import net.minecraft.util.Identifier;

import java.util.concurrent.CompletableFuture;

public abstract class IdRetainingRecipesDataProvider extends FabricRecipeProvider {
    public IdRetainingRecipesDataProvider(
        FabricDataOutput output, CompletableFuture<HolderLookup.Provider> registriesFuture
    ) {
        super(output, registriesFuture);
    }

    /**
     * Use the {@link Identifier id} provided for each recipe without
     * normalizing the namespace to the {@code "id"} field in {@code fabric.mod.json}
     */
    @Override
    protected Identifier getRecipeIdentifier(Identifier id) {
        return id;
    }
}
