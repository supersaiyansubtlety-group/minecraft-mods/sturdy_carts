package net.sssubtlety.sturdy_vehicles.datagen.recipe;

import net.sssubtlety.sturdy_vehicles.Init.ContentAccess;

import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput;

import net.minecraft.data.server.RecipesProvider;
import net.minecraft.data.server.recipe.RecipeExporter;
import net.minecraft.registry.HolderLookup;

import java.util.concurrent.CompletableFuture;

public abstract class ContentAccessingRecipesProvider extends RecipesProvider {
    protected final ContentAccess access;

    public ContentAccessingRecipesProvider(
        HolderLookup.Provider registries, RecipeExporter exporter,
        ContentAccess access
    ) {
        super(registries, exporter);

        this.access = access;
    }

    // public static abstract class DataProvider
    //     extends IdRetainingRecipesDataProvider {
    //     public DataProvider(FabricDataOutput output, CompletableFuture<HolderLookup.Provider> registriesFuture) {
    //         super(output, registriesFuture);
    //     }
    //
    //     // public static <P extends ContentAccessingRecipesProvider>
    //     //         RegistryDependentFactory<DataProvider<P>> toRegistryDependentFactory(
    //     //     DataProvider<P> dataProvider, ContentAccess access
    //     // ) {
    //     //     return dataProvider.toRegistryDependentFactory(access);
    //     // }
    //
    //     // public abstract DataProvider<P> create(
    //     //     FabricDataOutput output, CompletableFuture<HolderLookup. Provider> registriesFuture,
    //     //     ContentAccess access
    //     // );
    //
    //     // public RegistryDependentFactory<DataProvider<P>> toRegistryDependentFactory(
    //     //     ContentAccess access
    //     // ) {
    //     //     return (output, registriesFuture) -> this.create(output, registriesFuture, access);
    //     // }
    //
    //     // @FunctionalInterface
    //     // public interface Factory<P extends ContentAccessingRecipesProvider, D extends DataProvider<P>> {
    //     //     D create(
    //     //         FabricDataOutput output, CompletableFuture<HolderLookup. Provider> registriesFuture,
    //     //         ContentAccess access
    //     //     );
    //     // }
    // }
}
