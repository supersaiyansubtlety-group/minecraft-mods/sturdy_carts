package net.sssubtlety.sturdy_vehicles.datagen;

import net.sssubtlety.sturdy_vehicles.Init;
import net.sssubtlety.sturdy_vehicles.datagen.creator.FragileCartsCreator;
import net.sssubtlety.sturdy_vehicles.datagen.creator.FragileChestBoatsCreator;
import net.sssubtlety.sturdy_vehicles.datagen.creator.FragileOldBoatsCreator;
import net.sssubtlety.sturdy_vehicles.datagen.creator.FragileOldCartsCreator;
import net.sssubtlety.sturdy_vehicles.datagen.creator.KeepChestBoatNamesCreator;
import net.sssubtlety.sturdy_vehicles.datagen.creator.KeepMinecartNamesCreator;
import net.sssubtlety.sturdy_vehicles.datagen.creator.UncraftChestBoatsCreator;
import net.sssubtlety.sturdy_vehicles.datagen.creator.UncraftMinecartsCreator;

import net.fabricmc.fabric.api.datagen.v1.DataGeneratorEntrypoint;
import net.fabricmc.fabric.api.datagen.v1.FabricDataGenerator;

import java.util.stream.Stream;

public class DataGen implements DataGeneratorEntrypoint {
    @Override
    public void onInitializeDataGenerator(FabricDataGenerator generator) {
        final FabricDataGenerator.Pack modData = generator.createPack();
        modData.addProvider(SupportedTagProvider::new);

        Init.accessRegisteredContent(access ->
            Stream.of(
                new KeepMinecartNamesCreator(access),
                new KeepChestBoatNamesCreator(access),

                new UncraftMinecartsCreator(access),
                new UncraftChestBoatsCreator(access),

                new FragileCartsCreator(),
                new FragileOldCartsCreator(),

                new FragileChestBoatsCreator(),
                new FragileOldBoatsCreator()
            ).forEach(packCreator -> packCreator.create(generator))
        );
    }
}
