package net.sssubtlety.sturdy_vehicles;

import net.minecraft.entity.EntityType;
import net.minecraft.loot.LootTable;
import net.minecraft.registry.Registries;
import net.minecraft.registry.RegistryKey;
import net.minecraft.registry.RegistryKeys;

public class LootKeys {
    public static final RegistryKey<LootTable> MINECART = keyOf(EntityType.MINECART);
    public static final RegistryKey<LootTable> CHEST_MINECART = keyOf(EntityType.CHEST_MINECART);
    public static final RegistryKey<LootTable> FURNACE_MINECART = keyOf(EntityType.FURNACE_MINECART);
    public static final RegistryKey<LootTable> HOPPER_MINECART = keyOf(EntityType.HOPPER_MINECART);
    public static final RegistryKey<LootTable> TNT_MINECART = keyOf(EntityType.TNT_MINECART);

    // no builtin loot tables
    @SuppressWarnings("unused")
    public static final RegistryKey<LootTable> SPAWNER_MINECART = keyOf(EntityType.SPAWNER_MINECART);
    @SuppressWarnings("unused")
    public static final RegistryKey<LootTable> COMMAND_BLOCK_MINECART = keyOf(EntityType.COMMAND_BLOCK_MINECART);

    public static final RegistryKey<LootTable> ACACIA_CHEST_BOAT = keyOf(EntityType.ACACIA_CHEST_BOAT);
    public static final RegistryKey<LootTable> BAMBOO_CHEST_RAFT = keyOf(EntityType.BAMBOO_CHEST_RAFT);
    public static final RegistryKey<LootTable> BIRCH_CHEST_BOAT = keyOf(EntityType.BIRCH_CHEST_BOAT);
    public static final RegistryKey<LootTable> CHERRY_CHEST_BOAT = keyOf(EntityType.CHERRY_CHEST_BOAT);
    public static final RegistryKey<LootTable> DARK_OAK_CHEST_BOAT = keyOf(EntityType.DARK_OAK_CHEST_BOAT);
    public static final RegistryKey<LootTable> JUNGLE_CHEST_BOAT = keyOf(EntityType.JUNGLE_CHEST_BOAT);
    public static final RegistryKey<LootTable> MANGROVE_CHEST_BOAT = keyOf(EntityType.MANGROVE_CHEST_BOAT);
    public static final RegistryKey<LootTable> OAK_CHEST_BOAT = keyOf(EntityType.OAK_CHEST_BOAT);
    public static final RegistryKey<LootTable> PALE_OAK_CHEST_BOAT = keyOf(EntityType.PALE_OAK_CHEST_BOAT);
    public static final RegistryKey<LootTable> SPRUCE_CHEST_BOAT = keyOf(EntityType.SPRUCE_CHEST_BOAT);

    public static final RegistryKey<LootTable> ACACIA_BOAT = keyOf(EntityType.ACACIA_BOAT);
    public static final RegistryKey<LootTable> BAMBOO_RAFT = keyOf(EntityType.BAMBOO_RAFT);
    public static final RegistryKey<LootTable> BIRCH_BOAT = keyOf(EntityType.BIRCH_BOAT);
    public static final RegistryKey<LootTable> CHERRY_BOAT = keyOf(EntityType.CHERRY_BOAT);
    public static final RegistryKey<LootTable> DARK_OAK_BOAT = keyOf(EntityType.DARK_OAK_BOAT);
    public static final RegistryKey<LootTable> JUNGLE_BOAT = keyOf(EntityType.JUNGLE_BOAT);
    public static final RegistryKey<LootTable> MANGROVE_BOAT = keyOf(EntityType.MANGROVE_BOAT);
    public static final RegistryKey<LootTable> OAK_BOAT = keyOf(EntityType.OAK_BOAT);
    public static final RegistryKey<LootTable> PALE_OAK_BOAT = keyOf(EntityType.PALE_OAK_BOAT);
    public static final RegistryKey<LootTable> SPRUCE_BOAT = keyOf(EntityType.SPRUCE_BOAT);

    private static RegistryKey<LootTable> keyOf(EntityType<?> entityType) {
        return RegistryKey.of(
            RegistryKeys.LOOT_TABLE,
            Registries.ENTITY_TYPE.getId(entityType).withPrefix("entities/")
        );
    }
}
