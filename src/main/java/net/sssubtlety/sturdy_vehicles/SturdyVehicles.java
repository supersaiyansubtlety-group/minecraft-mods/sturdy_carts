package net.sssubtlety.sturdy_vehicles;

import org.jetbrains.annotations.ApiStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.minecraft.entity.EntityType;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.util.Identifier;

public class SturdyVehicles {
	public static final String NAMESPACE = "sturdy_vehicles";

	@Deprecated(forRemoval = true)
	@ApiStatus.ScheduledForRemoval(inVersion = "Minecraft 1.22")
    public static final String LEGACY_NAMESPACE = "sturdy_carts";

	public static final Logger LOGGER = LoggerFactory.getLogger(NAMESPACE);

	public static final TagKey<EntityType<?>> SUPPORTED = TagKey.of(RegistryKeys.ENTITY_TYPE, idOf("supported"));

	public static Identifier idOf(String path) {
		return Identifier.of(NAMESPACE, path);
	}

	public interface PackIds {
		Identifier FRAGILE_CARTS = idOf("fragile_carts");
		Identifier FRAGILE_OLD_CARTS = idOf("fragile_old_carts");

		Identifier FRAGILE_CHEST_BOATS = idOf("fragile_chest_boats");
		Identifier FRAGILE_OLD_BOATS = idOf("fragile_old_boats");

		Identifier KEEP_MINECART_NAMES = idOf("keep_minecart_names");
		Identifier KEEP_CHEST_BOAT_NAMES = idOf("keep_chest_boat_names");

		Identifier UNCRAFT_MINECARTS = idOf("uncraft_minecarts");
		Identifier UNCRAFT_CHEST_BOATS = idOf("uncraft_chest_boats");
	}
}
