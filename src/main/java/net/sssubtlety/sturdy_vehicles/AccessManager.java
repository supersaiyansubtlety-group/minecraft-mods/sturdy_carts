package net.sssubtlety.sturdy_vehicles;

import com.mojang.datafixers.util.Either;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Manages access to an object which will be supplied some time after the manager has been created.
 */
public class AccessManager<T> {
    /**
     * Holds an object to be accessed if {@link #setAccessible} has received one,
     * otherwise holds {@linkplain Consumer consumers} waiting to access the object that
     * {@link #setAccessible} will receive.
     */
    private Either<T, List<Consumer<T>>> holder = Either.right(new LinkedList<>());

    /**
     * <p>Accesses this manager's held object.
     *
     * <p>If {@link #setAccessible} has already received an object, immediately accesses that object.<br />
     * Otherwise, accesses the object supplied to {@link #setAccessible} as soon as it's received.
     */
    public void access(Consumer<T> accessor) {
        this.holder
            .ifLeft(accessor)
            .ifRight(accessors -> accessors.add(accessor));
    }

    /**
     * <p>Supplies the object to be accessed and grants any pending accessors access to it.
     * <p>Must only be called once.
     * @param resetFailMessage the message of the exception thrown if this manager has already received an object
     * @param objectSupplier supplier of the object to be accessed. Only called if an object has not yet been received.
     * @throws IllegalStateException if an object has already been supplied to this manager
     */
    public void setAccessible(String resetFailMessage, Supplier<T> objectSupplier) {
        this.holder
            .ifLeft(priorObject -> { throw new IllegalStateException(resetFailMessage); })
            .ifRight(accessors -> {
                final T object = objectSupplier.get();
                accessors.forEach(accessor -> accessor.accept(object));
                this.holder = Either.left(object);
            });
    }
}
