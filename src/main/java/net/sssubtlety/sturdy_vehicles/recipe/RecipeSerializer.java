package net.sssubtlety.sturdy_vehicles.recipe;

import net.minecraft.network.RegistryByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.recipe.Recipe;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;

import com.mojang.serialization.MapCodec;

public abstract class RecipeSerializer<R extends Recipe<?>> implements net.minecraft.recipe.RecipeSerializer<R> {
    private final Identifier id;
    private final MapCodec<R> codec;
    private final PacketCodec<RegistryByteBuf, R> packetCodec;

    public RecipeSerializer(Identifier id, MapCodec<R> codec, PacketCodec<RegistryByteBuf, R> packetCodec) {
        this.id = id;
        this.codec = codec;
        this.packetCodec = packetCodec;
    }

    @Override
    public MapCodec<R> getCodec() {
        return this.codec;
    }

    @Override
    public PacketCodec<RegistryByteBuf, R> getPacketCodec() {
        return this.packetCodec;
    }

    public void register() {
        Registry.register(Registries.RECIPE_SERIALIZER, this.id, this);
    }
}
