package net.sssubtlety.sturdy_vehicles.recipe;

import net.minecraft.component.DataComponentMap;
import net.minecraft.item.ItemStack;
import net.minecraft.network.RegistryByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.network.codec.PacketCodecs;
import net.minecraft.recipe.CraftingCategory;
import net.minecraft.recipe.CraftingRecipe;
import net.minecraft.recipe.display.RecipeDisplay;

import com.mojang.serialization.Codec;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import java.util.List;

import static net.minecraft.component.DataComponentTypes.CUSTOM_NAME;
import static net.minecraft.component.DataComponentTypes.LORE;

public abstract class BaseRecipe implements CraftingRecipe {
    public static ItemStack copyDestWithSourceNameAndLore(ItemStack source, ItemStack dest) {
        final var destCopy = dest.copy();
        final var copiedComponents = DataComponentMap.builder();

        final var sourceComponents = source.getComponents();
        final var sourceCustomName = sourceComponents.get(CUSTOM_NAME);
        final var sourceLore = sourceComponents.get(LORE);

        if (sourceCustomName != null) {
            copiedComponents.put(CUSTOM_NAME, sourceCustomName);
        }

        if (sourceLore != null) {
            copiedComponents.put(LORE, sourceLore);
        }

        destCopy.applyComponents(copiedComponents.build());

        return destCopy;
    }

    public final CommonProps commonProps;

    public BaseRecipe(CommonProps props) {
        this.commonProps = props;
    }

    @Override
    public String getGroup() {
        return this.commonProps.group;
    }

    @Override
    public CraftingCategory getCategory() {
        return this.commonProps.category;
    }

    @Override
    public boolean showNotification() {
        return this.commonProps.showNotification;
    }

    @Override
    public abstract List<RecipeDisplay> getDisplays();

    public record CommonProps(
        ItemStack result,
        String group,
        CraftingCategory category,
        boolean showNotification
    ) {
        private static final String DEFAULT_GROUP = "";
        private static final boolean DEFAULT_SHOW_NOTIFICATION = true;

        public static final MapCodec<CommonProps> CODEC = RecordCodecBuilder.mapCodec(
            instance -> instance.group(
                ItemStack.field_51397.fieldOf(Keys.RESULT)
                    .forGetter(CommonProps::result),
                Codec.STRING.optionalFieldOf(Keys.GROUP, DEFAULT_GROUP)
                    .forGetter(CommonProps::group),
                CraftingCategory.CODEC.fieldOf(Keys.CATEGORY).orElse(CraftingCategory.MISC)
                    .forGetter(CommonProps::category),
                Codec.BOOL.optionalFieldOf(Keys.SHOW_NOTIFICATION, DEFAULT_SHOW_NOTIFICATION)
                    .forGetter(CommonProps::showNotification)
            ).apply(instance, CommonProps::new)
        );

        public static final PacketCodec<RegistryByteBuf, CommonProps> PACKET_CODEC = PacketCodec.tuple(
            ItemStack.PACKET_CODEC,
            CommonProps::result,

            PacketCodecs.STRING,
            CommonProps::group,

            CraftingCategory.PACKET_CODEC,
            CommonProps::category,

            PacketCodecs.BOOL,
            CommonProps::showNotification,

            CommonProps::new
        );

        public CommonProps(ItemStack result, CraftingCategory category) {
            this(result, DEFAULT_GROUP, category, DEFAULT_SHOW_NOTIFICATION);
        }

        public interface Keys {
            String RESULT = "result";
            String GROUP = "group";
            String CATEGORY = "category";
            String SHOW_NOTIFICATION = "show_notification";
        }
    }
}
