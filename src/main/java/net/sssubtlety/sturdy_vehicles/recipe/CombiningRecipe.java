package net.sssubtlety.sturdy_vehicles.recipe;

import net.sssubtlety.sturdy_vehicles.SturdyVehicles;

import com.google.common.collect.ImmutableList;
import org.jetbrains.annotations.Nullable;

import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.network.RegistryByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.recipe.CraftingCategory;
import net.minecraft.recipe.CraftingRecipeInput;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.IngredientPlacement;
import net.minecraft.recipe.display.RecipeDisplay;
import net.minecraft.recipe.display.ShapedCraftingRecipeDisplay;
import net.minecraft.recipe.display.SlotDisplay;
import net.minecraft.registry.HolderLookup;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;

import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;

public class CombiningRecipe extends BaseRecipe {
    public static final Identifier ID = Identifier.of(SturdyVehicles.NAMESPACE, "combining");

    public static final MapCodec<CombiningRecipe> CODEC = codecOf(CombiningRecipe::new, Props.CODEC);

    public static final PacketCodec<RegistryByteBuf, CombiningRecipe> PACKET_CODEC = PacketCodec.tuple(
        CommonProps.PACKET_CODEC,
        recipe -> recipe.commonProps,

        Props.PACKET_CODEC,
        recipe -> recipe.props,

        CombiningRecipe::new
    );

    protected static final Serializer<CombiningRecipe> SERIALIZER =
        new Serializer<>(ID, CODEC, PACKET_CODEC, CombiningRecipe::new);

    public static Serializer<? extends CombiningRecipe> registerSerializer() {
        SERIALIZER.register();
        return SERIALIZER;
    }

    protected static CombiningRecipe of(CommonProps commonProps, Props props) {
        return new CombiningRecipe(commonProps, props);
    }

    protected static <R extends CombiningRecipe> MapCodec<R> codecOf(
        BiFunction<CommonProps, Props, R> factory, MapCodec<Props> propsCodec
    ) {
        return Codec.mapPair(CommonProps.CODEC, propsCodec).xmap(
            propsPair -> factory.apply(
                propsPair.getFirst(),
                propsPair.getSecond()
            ),
            recipe -> new Pair<>(
                recipe.commonProps,
                recipe.props
            )
        );
    }

    public final Props props;

    private final ImmutableList<Ingredient> ingredients;
    @Nullable
    private IngredientPlacement placement;

    protected CombiningRecipe(CommonProps commonProps, Props props) {
        super(commonProps);
        this.props = props;
        this.ingredients = ImmutableList.of(props.addition, props.base);
    }

    @Override
    public boolean matches(CraftingRecipeInput inventory, World world) {
        return this.findInputs(inventory).filter(inputs ->
            this.props.addition.test(inputs.getFirst())
                && this.props.base.test(inputs.getSecond())
        ).isPresent();
    }

    @Override
    public IngredientPlacement getIngredientPlacement() {
        if (this.placement == null) {
            this.placement = IngredientPlacement.create(this.ingredients);
        }

        return this.placement;
    }

    @Override
    public List<RecipeDisplay> getDisplays() {
        return List.of(new ShapedCraftingRecipeDisplay(
            1, 2,
            this.ingredients.stream().map(Ingredient::getSlotDisplay).toList(),
            new SlotDisplay.StackSlotDisplay(this.commonProps.result()),
            new SlotDisplay.ItemSlotDisplay(Items.CRAFTING_TABLE)
        ));
    }

    @Override
    public Serializer<? extends CombiningRecipe> getSerializer() {
        return SERIALIZER;
    }

    @Override
    public ItemStack craft(CraftingRecipeInput inventory, HolderLookup.Provider provider) {
        return copyDestWithSourceNameAndLore(
            this.findInputs(inventory).orElseThrow().getSecond(),
            this.commonProps.result()
        );
    }

    private Optional<Pair<ItemStack, ItemStack>> findInputs(CraftingRecipeInput inventory) {
        if (inventory.getInputCount() != 2) {
            return Optional.empty();
        }

        final int width = inventory.getWidth();
        if (width < 1 || inventory.getHeight() < 2) {
            return Optional.empty();
        }

        final int startOfLastRow = inventory.getSize() - width;
        for (int i = 0; i < startOfLastRow; i++) {
            final ItemStack topInput = inventory.get(i);
            if (!topInput.isEmpty()) {
                final ItemStack bottomInput = inventory.get(i + width);

                return bottomInput.isEmpty() ?
                    Optional.empty() :
                    Optional.of(new Pair<>(topInput, bottomInput));
            }
        }

        // this should never be reached because input count is checked at the top of the method
        return Optional.empty();
    }

    public record Props(Ingredient addition, Ingredient base) {
        public static final MapCodec<Props> CODEC = codecOfKeys(Keys.ADDITION, Keys.BASE);

        public static final PacketCodec<RegistryByteBuf, Props> PACKET_CODEC = PacketCodec.tuple(
            Ingredient.PACKET_CODEC,
            Props::addition,

            Ingredient.PACKET_CODEC,
            Props::base,

            Props::new
        );

        public static MapCodec<Props> codecOfKeys(String additionKey, String baseKey) {
            return RecordCodecBuilder.mapCodec(
                instance -> instance.group(
                    Ingredient.ALLOW_EMPTY_CODEC.fieldOf(additionKey).forGetter(Props::addition),
                    Ingredient.ALLOW_EMPTY_CODEC.fieldOf(baseKey).forGetter(Props::base)
                ).apply(instance, Props::new)
            );
        }

        public interface Keys {
            String ADDITION = "addition";
            String BASE = "base";
        }
    }

    public static class Serializer<R extends CombiningRecipe> extends RecipeSerializer<R> {
        private final BiFunction<CommonProps, Props, R> factory;

        protected Serializer(
            Identifier id,
            MapCodec<R> codec, PacketCodec<RegistryByteBuf, R> packetCodec,
            BiFunction<CommonProps, Props, R> factory
        ) {
            super(id, codec, packetCodec);
            this.factory = factory;
        }

        public R recipeOf(
            ItemStack result, String group, CraftingCategory category, boolean showNotification,
            Ingredient addition, Ingredient base
        ) {
            return this.factory.apply(
                new CommonProps(result, group, category, showNotification),
                new Props(addition, base)
            );
        }

        public R recipeOf(
            Ingredient addition, Ingredient base, ItemStack result, CraftingCategory category
        ) {
            return this.factory.apply(
                new CommonProps(result, category),
                new Props(addition, base)
            );
        }
    }
}
