package net.sssubtlety.sturdy_vehicles.recipe;

import net.sssubtlety.sturdy_vehicles.SturdyVehicles;

import org.jetbrains.annotations.ApiStatus;

import net.minecraft.network.RegistryByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.util.Identifier;

import com.mojang.serialization.MapCodec;

@Deprecated(forRemoval = true)
@ApiStatus.ScheduledForRemoval(inVersion = "Minecraft 1.22")
@SuppressWarnings("DeprecatedIsStillUsed")
public class MinecartUncraftingRecipe extends SplittingRecipe {
    @SuppressWarnings("removal")
    public static final Identifier ID = Identifier.of(SturdyVehicles.LEGACY_NAMESPACE, "minecart_uncrafting");

    public static final MapCodec<MinecartUncraftingRecipe> CODEC = codecOf(MinecartUncraftingRecipe::new, Props.CODEC);

    public static final PacketCodec<RegistryByteBuf, MinecartUncraftingRecipe> PACKET_CODEC =
        SplittingRecipe.PACKET_CODEC.map(
            splittingRecipe -> new MinecartUncraftingRecipe(splittingRecipe.commonProps, splittingRecipe.props),
            recipe -> SplittingRecipe.of(recipe.commonProps, recipe.props)
        );

    protected static final Serializer<MinecartUncraftingRecipe> SERIALIZER =
        new Serializer<>(ID, CODEC, PACKET_CODEC, MinecartUncraftingRecipe::new);

    @SuppressWarnings("UnusedReturnValue")
    public static Serializer<? extends SplittingRecipe> registerSerializer() {
        SERIALIZER.register();

        return SERIALIZER;
    }

    protected static MinecartUncraftingRecipe of(CommonProps commonProps, SplittingRecipe.Props props) {
        return new MinecartUncraftingRecipe(commonProps, props);
    }

    protected MinecartUncraftingRecipe(CommonProps commonProps, SplittingRecipe.Props props) {
        super(commonProps, props);
    }

    @Override
    public Serializer<? extends SplittingRecipe> getSerializer() {
        return SERIALIZER;
    }

    public interface Props {
        MapCodec<SplittingRecipe.Props> CODEC =
            SplittingRecipe.Props.codecOfKeys(Keys.MINECART_COMPOSITE, Keys.MINECART);

        interface Keys {
            String MINECART_COMPOSITE = "minecart_composite";
            String MINECART = "minecart";
        }
    }
}
