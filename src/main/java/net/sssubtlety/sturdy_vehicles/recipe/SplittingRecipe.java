package net.sssubtlety.sturdy_vehicles.recipe;

import net.sssubtlety.sturdy_vehicles.SturdyVehicles;

import org.jetbrains.annotations.Nullable;

import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.network.RegistryByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.recipe.CraftingCategory;
import net.minecraft.recipe.CraftingRecipeInput;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.IngredientPlacement;
import net.minecraft.recipe.display.RecipeDisplay;
import net.minecraft.recipe.display.ShapelessCraftingRecipeDisplay;
import net.minecraft.recipe.display.SlotDisplay;
import net.minecraft.registry.HolderLookup;
import net.minecraft.util.Identifier;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.world.World;

import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;

public class SplittingRecipe extends BaseRecipe {
    public static final Identifier ID = Identifier.of(SturdyVehicles.NAMESPACE, "splitting");

    public static final MapCodec<SplittingRecipe> CODEC = codecOf(SplittingRecipe::new, Props.CODEC);

    public static final PacketCodec<RegistryByteBuf, SplittingRecipe> PACKET_CODEC = PacketCodec.tuple(
        CommonProps.PACKET_CODEC,
        recipe -> recipe.commonProps,

        Props.PACKET_CODEC,
        recipe -> recipe.props,

        SplittingRecipe::new
    );

    protected static final Serializer<SplittingRecipe> SERIALIZER =
        new Serializer<>(ID, CODEC, PACKET_CODEC, SplittingRecipe::new);

    public static Serializer<? extends SplittingRecipe> registerSerializer() {
        SERIALIZER.register();
        return SERIALIZER;
    }

    protected static SplittingRecipe of(CommonProps commonProps, Props props) {
        return new SplittingRecipe(commonProps, props);
    }

    protected static <R extends SplittingRecipe> MapCodec<R> codecOf(
        BiFunction<CommonProps, Props, R> factory, MapCodec<Props> propsCodec
    ) {
        return Codec.mapPair(CommonProps.CODEC, propsCodec)
            .xmap(
                propsPair -> factory.apply(
                    propsPair.getFirst(),
                    propsPair.getSecond()
                ),
                recipe -> new Pair<>(
                    recipe.commonProps,
                    recipe.props
                )
            );
    }

    public final Props props;
    @Nullable
    private IngredientPlacement placement;

    protected SplittingRecipe(CommonProps commonProps, Props props) {
        super(commonProps);
        this.props = props;
    }

    @Override
    public boolean matches(CraftingRecipeInput inventory, World world) {
        return this.findInput(inventory).filter(this.props.composite).isPresent();
    }

    @Override
    public DefaultedList<ItemStack> getRecipeRemainders(CraftingRecipeInput inventory) {
        final int inputIndex = this.findInputIndex(inventory).orElseThrow();

        final DefaultedList<ItemStack> remainders = super.getRecipeRemainders(inventory);
        remainders.set(inputIndex, copyDestWithSourceNameAndLore(inventory.get(inputIndex), this.props.base));
        return remainders;
    }

    @Override
    public IngredientPlacement getIngredientPlacement() {
        if (this.placement == null) {
            this.placement = IngredientPlacement.create(this.props.composite);
        }

        return this.placement;
    }

    @Override
    public List<RecipeDisplay> getDisplays() {
        return List.of(new ShapelessCraftingRecipeDisplay(
            List.of(this.props.composite.getSlotDisplay()),
            new SlotDisplay.StackSlotDisplay(this.commonProps.result()),
            new SlotDisplay.ItemSlotDisplay(Items.CRAFTING_TABLE)
        ));
    }

    @Override
    public Serializer<? extends SplittingRecipe> getSerializer() {
        return SERIALIZER;
    }

    private Optional<ItemStack> findInput(CraftingRecipeInput inventory) {
        return this.findInputIndex(inventory).map(inventory::get);
    }

    private Optional<Integer> findInputIndex(CraftingRecipeInput inventory) {
        if (inventory.getInputCount() != 1) {
            return Optional.empty();
        }

        for (int i = 0; i < inventory.getSize(); i++) {
            if (!inventory.get(i).isEmpty()) {
                return Optional.of(i);
            }
        }

        // this should never be reached because input count is checked at the top
        return Optional.empty();
    }

    @Override
    public ItemStack craft(CraftingRecipeInput inventory, HolderLookup.Provider provider) {
        return this.commonProps.result().copy();
    }

    public record Props(Ingredient composite, ItemStack base) {
        public static final MapCodec<Props> CODEC = codecOfKeys(Keys.COMPOSITE, Keys.BASE);

        public static MapCodec<Props> codecOfKeys(String compositeKey, String baseKey) {
            return RecordCodecBuilder.mapCodec(
                instance -> instance.group(
                    Ingredient.ALLOW_EMPTY_CODEC.fieldOf(compositeKey).forGetter(Props::composite),
                    ItemStack.field_51397.fieldOf(baseKey).forGetter(Props::base)
                ).apply(instance, Props::new)
            );
        }

        public static final PacketCodec<RegistryByteBuf, Props> PACKET_CODEC = PacketCodec.tuple(
            Ingredient.PACKET_CODEC,
            Props::composite,

            ItemStack.PACKET_CODEC,
            Props::base,

            Props::new
        );

        public interface Keys {
            String COMPOSITE = "composite";
            String BASE = "base";
        }
    }

    public static class Serializer<R extends SplittingRecipe> extends RecipeSerializer<R> {
        private final BiFunction<CommonProps, Props, R> factory;

        public Serializer(
            Identifier id,
            MapCodec<R> codec, PacketCodec<RegistryByteBuf, R> packetCodec,
            BiFunction<CommonProps, Props, R> factory
        ) {
            super(id, codec, packetCodec);
            this.factory = factory;
        }

        public R recipeOf(
            ItemStack result, String group, CraftingCategory category, boolean showNotification,
            Ingredient composite, ItemStack base
        ) {
            return this.factory.apply(
                new CommonProps(result, group, category, showNotification),
                new Props(composite, base)
            );
        }

        public R recipeOf(
            Ingredient composite, ItemStack base, ItemStack result, CraftingCategory category
        ) {
            return this.factory.apply(
                new CommonProps(result, category),
                new Props(composite, base)
            );
        }
    }
}
