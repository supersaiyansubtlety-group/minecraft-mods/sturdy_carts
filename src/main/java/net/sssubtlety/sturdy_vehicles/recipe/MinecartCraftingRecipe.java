package net.sssubtlety.sturdy_vehicles.recipe;

import net.sssubtlety.sturdy_vehicles.SturdyVehicles;

import org.jetbrains.annotations.ApiStatus;

import net.minecraft.network.RegistryByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.util.Identifier;

import com.mojang.serialization.MapCodec;

@Deprecated(forRemoval = true)
@ApiStatus.ScheduledForRemoval(inVersion = "Minecraft 1.22")
@SuppressWarnings("DeprecatedIsStillUsed")
public class MinecartCraftingRecipe extends CombiningRecipe {
    @SuppressWarnings("removal")
    public static final Identifier ID = Identifier.of(SturdyVehicles.LEGACY_NAMESPACE, "minecart_crafting");

    public static final MapCodec<MinecartCraftingRecipe> CODEC = codecOf(MinecartCraftingRecipe::new, Props.CODEC);

    public static final PacketCodec<RegistryByteBuf, MinecartCraftingRecipe> PACKET_CODEC =
        CombiningRecipe.PACKET_CODEC.map(
            combiningRecipe -> new MinecartCraftingRecipe(combiningRecipe.commonProps, combiningRecipe.props),
            recipe -> CombiningRecipe.of(recipe.commonProps, recipe.props)
        );

    protected static final Serializer<MinecartCraftingRecipe> SERIALIZER =
        new Serializer<>(ID, CODEC, PACKET_CODEC, MinecartCraftingRecipe::new);

    @SuppressWarnings("UnusedReturnValue")
    public static Serializer<? extends CombiningRecipe> registerSerializer() {
        SERIALIZER.register();

        return SERIALIZER;
    }

    protected static MinecartCraftingRecipe of(CommonProps commonProps, CombiningRecipe.Props props) {
        return new MinecartCraftingRecipe(commonProps, props);
    }

    protected MinecartCraftingRecipe(CommonProps commonProps, CombiningRecipe.Props props) {
        super(commonProps, props);
    }

    @Override
    public Serializer<? extends CombiningRecipe> getSerializer() {
        return SERIALIZER;
    }

    public interface Props {
        MapCodec<CombiningRecipe.Props> CODEC = CombiningRecipe.Props.codecOfKeys(Keys.ADDITION, Keys.MINECART);

        interface Keys {
            String ADDITION = "addition";
            String MINECART = "minecart";
        }
    }
}
