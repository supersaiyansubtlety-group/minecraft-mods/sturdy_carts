package net.sssubtlety.sturdy_vehicles;

import net.sssubtlety.sturdy_vehicles.SturdyVehicles.PackIds;
import net.sssubtlety.sturdy_vehicles.recipe.CombiningRecipe;
import net.sssubtlety.sturdy_vehicles.recipe.SplittingRecipe;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.resource.ResourcePackActivationType;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;

import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

import java.util.function.Consumer;

import static net.fabricmc.fabric.api.resource.ResourceManagerHelper.registerBuiltinResourcePack;

public final class Init implements ModInitializer {
    private static final AccessManager<ContentAccess> CONTENT_ACCESS_MANAGER = new AccessManager<>();

    public static void accessRegisteredContent(Consumer<ContentAccess> accessor) {
        CONTENT_ACCESS_MANAGER.access(accessor);
    }

    private static void registerBuiltinPacks(
        ModContainer modContainer, ResourcePackActivationType activationType, Identifier... ids
    ) {
        for (final var id : ids) {
            registerBuiltinResourcePack(
                id,
                modContainer,
                packNameOf(id),
                activationType
            );
        }
    }

    private static Text packNameOf(Identifier packId) {
        return Text.translatable(
            String.join(".", "pack", packId.getNamespace(), packId.getPath(), "name")
        );
    }

    @Override
    public void onInitialize() {
        CONTENT_ACCESS_MANAGER.setAccessible("Trying to re-initialize!", () -> {
            FabricLoader.getInstance().getModContainer(SturdyVehicles.NAMESPACE).ifPresent(sturdyCarts -> {
                registerBuiltinPacks(
                    sturdyCarts, ResourcePackActivationType.DEFAULT_ENABLED,
                    PackIds.KEEP_MINECART_NAMES,
                    PackIds.UNCRAFT_MINECARTS,
                    PackIds.KEEP_CHEST_BOAT_NAMES,
                    PackIds.UNCRAFT_CHEST_BOATS
                );

                registerBuiltinPacks(
                    sturdyCarts, ResourcePackActivationType.NORMAL,
                    PackIds.FRAGILE_CARTS,
                    PackIds.FRAGILE_OLD_CARTS,
                    PackIds.FRAGILE_CHEST_BOATS,
                    PackIds.FRAGILE_OLD_BOATS
                );
            });

            registerLegacyRecipes();

            return new ContentAccess(
                CombiningRecipe.registerSerializer(),
                SplittingRecipe.registerSerializer()
            );
        });
    }

    @SuppressWarnings("removal")
    private static void registerLegacyRecipes() {
        net.sssubtlety.sturdy_vehicles.recipe.MinecartCraftingRecipe.registerSerializer();
        net.sssubtlety.sturdy_vehicles.recipe.MinecartUncraftingRecipe.registerSerializer();
    }

    public record ContentAccess(
        CombiningRecipe.Serializer<?> combiningSerializer,
        SplittingRecipe.Serializer<?> splittingSerializer
    ) { }
}
