package net.sssubtlety.sturdy_vehicles.mixin;

import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;

import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.vehicle.AbstractMinecartEntity;
import net.minecraft.entity.vehicle.TntMinecartEntity;
import net.minecraft.item.Item;
import net.minecraft.server.world.ServerWorld;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

@Mixin(TntMinecartEntity.class)
abstract class TntMinecartEntityMixin extends AbstractMinecartEntity {
    private TntMinecartEntityMixin() {
        //noinspection DataFlowIssue
        super(null, null);
        throw new IllegalStateException("Dummy constructor called");
    }

    // TntMinecartEntity overrides killAndDropSelf without calling super
    // replace killAndDropItem with super.killAndDropSelf(ServerWorld, DamageSource)
    @WrapOperation(
        method = "killAndDropSelf",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/entity/vehicle/TntMinecartEntity;killAndDropItem(" +
                "Lnet/minecraft/server/world/ServerWorld;Lnet/minecraft/item/Item;)V"
        )
    )
    protected void delegateToSuper(
        TntMinecartEntity instance, ServerWorld serverWorld, Item item, Operation<Void> original,
        ServerWorld world, DamageSource source
    ) {
        super.killAndDropSelf(world, source);
    }
}
