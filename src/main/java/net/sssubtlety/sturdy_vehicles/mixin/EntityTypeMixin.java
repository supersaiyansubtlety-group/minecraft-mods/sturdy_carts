package net.sssubtlety.sturdy_vehicles.mixin;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;

import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Slice;

@Mixin(value = EntityType.class)
abstract class EntityTypeMixin {
    @Unique private static final String CONSTANT = "CONSTANT";
    @Unique private static final String INVOKE = "INVOKE";
    @Unique private static final String STR_VAL = "stringValue=";
    @Unique private static final String EMPTY_LOOT_TABLE_REF =
        "Lnet/minecraft/entity/EntityType$Builder;emptyLootTable()Lnet/minecraft/entity/EntityType$Builder;";

    @Unique private static final String ACACIA_BOAT = "acacia_boat";
    @Unique private static final String ACACIA_CHEST_BOAT = "acacia_chest_boat";
    @Unique private static final String BAMBOO_RAFT = "bamboo_raft";
    @Unique private static final String BAMBOO_CHEST_RAFT = "bamboo_chest_raft";
    @Unique private static final String BIRCH_BOAT = "birch_boat";
    @Unique private static final String BIRCH_CHEST_BOAT = "birch_chest_boat";
    @Unique private static final String CHERRY_BOAT = "cherry_boat";
    @Unique private static final String CHERRY_CHEST_BOAT = "cherry_chest_boat";
    @Unique private static final String DARK_OAK_BOAT = "dark_oak_boat";
    @Unique private static final String DARK_OAK_CHEST_BOAT = "dark_oak_chest_boat";
    @Unique private static final String JUNGLE_BOAT = "jungle_boat";
    @Unique private static final String JUNGLE_CHEST_BOAT = "jungle_chest_boat";
    @Unique private static final String MANGROVE_BOAT = "mangrove_boat";
    @Unique private static final String MANGROVE_CHEST_BOAT = "mangrove_chest_boat";
    @Unique private static final String OAK_BOAT = "oak_boat";
    @Unique private static final String OAK_CHEST_BOAT = "oak_chest_boat";
    @Unique private static final String PALE_OAK_BOAT = "pale_oak_boat";
    @Unique private static final String PALE_OAK_CHEST_BOAT = "pale_oak_chest_boat";
    @Unique private static final String SPRUCE_BOAT = "spruce_boat";
    @Unique private static final String SPRUCE_CHEST_BOAT = "spruce_chest_boat";
    // minecarts
    @Unique private static final String MINECART = "minecart";
    @Unique private static final String CHEST_MINECART = "chest_minecart";
    @Unique private static final String FURNACE_MINECART = "furnace_minecart";
    @Unique private static final String HOPPER_MINECART = "hopper_minecart";
    @Unique private static final String TNT_MINECART = "tnt_minecart";
    // no included loot tables
    @Unique private static final String COMMAND_BLOCK_MINECART = "command_block_minecart";
    @Unique private static final String SPAWNER_MINECART = "spawner_minecart";

    @WrapOperation(
        method = "<clinit>", allow = 27, require = 27,
        slice = {
            @Slice(id = ACACIA_BOAT, from = @At(value = CONSTANT, args = STR_VAL + ACACIA_BOAT)),
            @Slice(id = ACACIA_CHEST_BOAT,from = @At(value = CONSTANT, args = STR_VAL + ACACIA_CHEST_BOAT)),
            @Slice(id = BAMBOO_RAFT,from = @At(value = CONSTANT, args = STR_VAL + BAMBOO_RAFT)),
            @Slice(id = BAMBOO_CHEST_RAFT,from = @At(value = CONSTANT, args = STR_VAL + BAMBOO_CHEST_RAFT)),
            @Slice(id = BIRCH_BOAT,from = @At(value = CONSTANT, args = STR_VAL + BIRCH_BOAT)),
            @Slice(id = BIRCH_CHEST_BOAT,from = @At(value = CONSTANT, args = STR_VAL + BIRCH_CHEST_BOAT)),
            @Slice(id = CHERRY_BOAT,from = @At(value = CONSTANT, args = STR_VAL + CHERRY_BOAT)),
            @Slice(id = CHERRY_CHEST_BOAT,from = @At(value = CONSTANT, args = STR_VAL + CHERRY_CHEST_BOAT)),
            @Slice(id = DARK_OAK_BOAT,from = @At(value = CONSTANT, args = STR_VAL + DARK_OAK_BOAT)),
            @Slice(id = DARK_OAK_CHEST_BOAT,from = @At(value = CONSTANT, args = STR_VAL + DARK_OAK_CHEST_BOAT)),
            @Slice(id = JUNGLE_BOAT,from = @At(value = CONSTANT, args = STR_VAL + JUNGLE_BOAT)),
            @Slice(id = JUNGLE_CHEST_BOAT,from = @At(value = CONSTANT, args = STR_VAL + JUNGLE_CHEST_BOAT)),
            @Slice(id = MANGROVE_BOAT,from = @At(value = CONSTANT, args = STR_VAL + MANGROVE_BOAT)),
            @Slice(id = MANGROVE_CHEST_BOAT,from = @At(value = CONSTANT, args = STR_VAL + MANGROVE_CHEST_BOAT)),
            @Slice(id = OAK_BOAT,from = @At(value = CONSTANT, args = STR_VAL + OAK_BOAT)),
            @Slice(id = OAK_CHEST_BOAT,from = @At(value = CONSTANT, args = STR_VAL + OAK_CHEST_BOAT)),
            @Slice(id = PALE_OAK_BOAT,from = @At(value = CONSTANT, args = STR_VAL + PALE_OAK_BOAT)),
            @Slice(id = PALE_OAK_CHEST_BOAT,from = @At(value = CONSTANT, args = STR_VAL + PALE_OAK_CHEST_BOAT)),
            @Slice(id = SPRUCE_BOAT,from = @At(value = CONSTANT, args = STR_VAL + SPRUCE_BOAT)),
            @Slice(id = SPRUCE_CHEST_BOAT,from = @At(value = CONSTANT, args = STR_VAL + SPRUCE_CHEST_BOAT)),
            // minecarts
            @Slice(id = MINECART,from = @At(value = CONSTANT, args = STR_VAL + MINECART)),
            @Slice(id = CHEST_MINECART,from = @At(value = CONSTANT, args = STR_VAL + CHEST_MINECART)),
            @Slice(id = FURNACE_MINECART,from = @At(value = CONSTANT, args = STR_VAL + FURNACE_MINECART)),
            @Slice(id = HOPPER_MINECART,from = @At(value = CONSTANT, args = STR_VAL + HOPPER_MINECART)),
            @Slice(id = TNT_MINECART,from = @At(value = CONSTANT, args = STR_VAL + TNT_MINECART)),
            // no included loot tables
            @Slice(id = COMMAND_BLOCK_MINECART,from = @At(value = CONSTANT, args = STR_VAL + COMMAND_BLOCK_MINECART)),
            @Slice(id = SPAWNER_MINECART,from = @At(value = CONSTANT, args = STR_VAL + SPAWNER_MINECART))
        },
        at = {
            @At(slice = ACACIA_BOAT, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = ACACIA_CHEST_BOAT, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = BAMBOO_RAFT, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = BAMBOO_CHEST_RAFT, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = BIRCH_BOAT, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = BIRCH_CHEST_BOAT, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = CHERRY_BOAT, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = CHERRY_CHEST_BOAT, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = DARK_OAK_BOAT, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = DARK_OAK_CHEST_BOAT, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = JUNGLE_BOAT, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = JUNGLE_CHEST_BOAT, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = MANGROVE_BOAT, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = MANGROVE_CHEST_BOAT, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = OAK_BOAT, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = OAK_CHEST_BOAT, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = PALE_OAK_BOAT, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = PALE_OAK_CHEST_BOAT, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = SPRUCE_BOAT, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = SPRUCE_CHEST_BOAT, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            // minecarts
            @At(slice = MINECART, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = CHEST_MINECART, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = FURNACE_MINECART, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = HOPPER_MINECART, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = TNT_MINECART, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            // no included loot tables
            @At(slice = COMMAND_BLOCK_MINECART, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF),
            @At(slice = SPAWNER_MINECART, value = INVOKE, ordinal = 0, target = EMPTY_LOOT_TABLE_REF)
        }
    )
    private static <E extends Entity> EntityType.Builder<E> keepLootTable(
        EntityType.Builder<E> instance, Operation<EntityType.Builder<E>> original
    ) {
        return instance;
    }
}
