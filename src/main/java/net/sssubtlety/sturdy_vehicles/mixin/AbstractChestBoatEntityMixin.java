package net.sssubtlety.sturdy_vehicles.mixin;

import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;

import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.vehicle.AbstractBoatEntity;
import net.minecraft.entity.vehicle.AbstractChestBoatEntity;
import net.minecraft.item.Item;
import net.minecraft.server.world.ServerWorld;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

@Mixin(AbstractChestBoatEntity.class)
abstract class AbstractChestBoatEntityMixin extends AbstractBoatEntity {
    private AbstractChestBoatEntityMixin() {
        //noinspection DataFlowIssue
        super(null, null, null);
        throw new IllegalStateException("Dummy constructor called");
    }

    // AbstractChestBoatEntity overrides killAndDropSelf without calling super
    // replace killAndDropItem with super.killAndDropSelf(ServerWorld, DamageSource)
    @WrapOperation(
        method = "killAndDropSelf",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/entity/vehicle/AbstractChestBoatEntity;killAndDropItem(" +
                "Lnet/minecraft/server/world/ServerWorld;Lnet/minecraft/item/Item;)V"
        )
    )
    protected void delegateToSuper(
        AbstractChestBoatEntity instance, ServerWorld serverWorld, Item item, Operation<Void> original,
        ServerWorld world, DamageSource source
    ) {
        super.killAndDropSelf(world, source);
    }
}
