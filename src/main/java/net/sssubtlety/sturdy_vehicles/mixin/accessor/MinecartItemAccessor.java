package net.sssubtlety.sturdy_vehicles.mixin.accessor;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.vehicle.AbstractMinecartEntity;
import net.minecraft.item.MinecartItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(MinecartItem.class)
public interface MinecartItemAccessor {
    @Accessor("type")
    EntityType<? extends AbstractMinecartEntity> sturdy_carts$getType();
}
