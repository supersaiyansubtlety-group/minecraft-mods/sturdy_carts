package net.sssubtlety.sturdy_vehicles.mixin.accessor;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.vehicle.AbstractBoatEntity;
import net.minecraft.item.BoatItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(BoatItem.class)
public interface BoatItemAccessor {
    @Accessor("field_54469")
    EntityType<? extends AbstractBoatEntity> sturdy_carts$getType();
}
