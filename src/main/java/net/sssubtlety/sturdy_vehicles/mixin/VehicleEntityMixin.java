package net.sssubtlety.sturdy_vehicles.mixin;

import net.sssubtlety.sturdy_vehicles.SturdyVehicles;
import net.sssubtlety.sturdy_vehicles.mixin.accessor.BoatItemAccessor;
import net.sssubtlety.sturdy_vehicles.mixin.accessor.MinecartItemAccessor;

import com.llamalad7.mixinextras.injector.ModifyExpressionValue;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.vehicle.AbstractBoatEntity;
import net.minecraft.entity.vehicle.AbstractMinecartEntity;
import net.minecraft.entity.vehicle.VehicleEntity;
import net.minecraft.item.BoatItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.MinecartItem;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.context.LootContextParameterSet;
import net.minecraft.loot.context.LootContextParameters;
import net.minecraft.loot.context.LootContextTypes;
import net.minecraft.server.world.ServerWorld;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Mixin(VehicleEntity.class)
abstract class VehicleEntityMixin extends Entity {
    @Unique
    private static Optional<EntityType<? extends Entity>> getMinecartType(Item item) {
        return item instanceof MinecartItem minecartItem ?
            Optional.of(((MinecartItemAccessor) minecartItem).sturdy_carts$getType()) :
            Optional.empty();
    }

    @Unique
    private static Optional<EntityType<? extends Entity>> getBoatType(Item item) {
        return item instanceof BoatItem boatItem ?
            Optional.of(((BoatItemAccessor) boatItem).sturdy_carts$getType()) :
            Optional.empty();
    }

    private VehicleEntityMixin() {
        //noinspection DataFlowIssue
        super(null, null);
        throw new IllegalStateException("Dummy constructor called");
    }

    @ModifyExpressionValue(
        method = "killAndDropSelf",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/entity/vehicle/VehicleEntity;asItem()Lnet/minecraft/item/Item;"
        )
    )
    protected Item tryReplaceMainItemAndDropLoot(Item original, ServerWorld world, DamageSource source) {
        return this.tryReplaceMainItemAndDropLootImpl(source, world).orElse(original);
    }

    /**
     * If {@code this} has a loot table: generates loot, then drops the loot except for the "main"
     * item, and returns the "main" item.<br />
     * The "main" item will be given {@code this} vehicle's name by the enclosing vanilla method.<br />
     *
     * @return
     * <ul>
     *     <li>{@link Optional#empty() empty} if the {@code this} has no
     *         {@linkplain Entity#getLootTable() loot table key}, or if the key is for {@link LootTable#EMPTY}
     *     <li>otherwise the first respective {@link Item} whose {@code itemVariantGetter}
     *         returns a matching {@code vehicleVariant} if found in loot
     *     <li>otherwise the first {@link Item} whose {@code itemVariantGetter} returns a
     *         non-{@linkplain Optional#empty() empty} variant
     *     <li>otherwise {@link Items#AIR AIR}; this prevents any item from receiving {@code this}'s name
     * </ul>
     */
    @Unique
    private Optional<Item> tryReplaceMainItemAndDropLootImpl(DamageSource source, ServerWorld world) {
        final VehicleEntity vehicle = (VehicleEntity) (Object) this;

        if (!vehicle.getType().isIn(SturdyVehicles.SUPPORTED)) {
            return Optional.empty();
        }

        return vehicle.getLootTable()
            .map(world.getServer().getReloadableRegistries()::getLootTable)
            .flatMap(lootTable -> {
                if (lootTable == LootTable.EMPTY) {
                    return Optional.empty();
                }

                final var lootParamsBuilder = new LootContextParameterSet.Builder(world)
                    .add(LootContextParameters.THIS_ENTITY, vehicle)
                    .add(LootContextParameters.ORIGIN, vehicle.getPos())
                    .add(LootContextParameters.DAMAGE_SOURCE, source)
                    .addOptional(LootContextParameters.ATTACKING_ENTITY, source.getAttacker())
                    .addOptional(LootContextParameters.DIRECT_ATTACKING_ENTITY, source.getSource());

                final List<ItemStack> loot = lootTable.generateLoot(lootParamsBuilder.build(LootContextTypes.ENTITY));

                final Function<Item, Optional<EntityType<? extends Entity>>> itemTypeGetter = switch (vehicle) {
                    case AbstractMinecartEntity ignored -> VehicleEntityMixin::getMinecartType;
                    case AbstractBoatEntity ignored -> VehicleEntityMixin::getBoatType;
                    default -> unused -> Optional.empty();
                };

                // Iterate over loot looking for vehicle Item that matches this type.
                // The first vehicle Item not of this type is also stored for use
                // if one of the correct type isn't found.
                Item matchingVehicleItem = null;
                ItemStack someVehicleStack = null;
                final var lootItr = loot.iterator();
                while (lootItr.hasNext()) {
                    final var stack = lootItr.next();
                    if (stack.getCount() == 1) {
                        final Item item = stack.getItem();
                        final Optional<?> itemType = itemTypeGetter.apply(item);
                        if (itemType.isPresent()) {
                            if (itemType.get() == vehicle.getType()) {
                                matchingVehicleItem = item;
                                lootItr.remove();
                                break;
                            } else if (someVehicleStack == null) {
                                someVehicleStack = stack;
                                lootItr.remove();
                                continue;
                            }
                        }
                    }

                    vehicle.dropStack(world, stack);
                }

                // drop any remaining items
                while (lootItr.hasNext()) {
                    vehicle.dropStack(world, lootItr.next());
                }

                // prefer matchingVehicleItem (matches this type)
                // fallback to someVehicleStack (a MinecartItem/BoatItem of another type)
                if (matchingVehicleItem == null) {
                    return Optional.of(someVehicleStack == null ?
                        Items.AIR :
                        someVehicleStack.getItem()
                    );
                } else {
                    if (someVehicleStack != null) {
                        vehicle.dropStack(world, someVehicleStack);
                    }

                    return Optional.of(matchingVehicleItem);
                }
            });
    }
}
