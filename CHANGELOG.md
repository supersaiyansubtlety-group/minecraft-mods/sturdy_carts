- 2.1.0 (15 Dec. 2024):
  - Updated for 1.21.2-1.21.4
  - Due to changes in vanilla, [Polymer](https://modrinth.com/mod/polymer/versions)
  is no longer required for server-side functionality
  - Note that added recipe types use vanilla's ingredient parsing,
  so recipes will need similar updates to vanilla recipes
  - Added support for Pale Oak Boats and Pale Oak Chest Boats
- 2.0.0 (13 Sep. 2024):
  - Marked as compatible with 1.21.1
  - Renamed to Sturdy Vehicles!
  - This mod has had an identity crisis since 1.19 made Minecarts drop themselves instead of breaking apart,
  this update resolves that:
    - Added support for changing Boat loot tables!
    - Builtin recipe data packs are now enabled by default
    - Migrated recipe type ids and keys
      - `"sturdy_carts:minecart_crafting"` is now `"sturdy_vehicles:combining"`
        - the `"minecart"` key is now `"base"`
      - `"sturdy_carts:minecart_uncrafting"` is now `"sturdy_vehicles:splitting"`
        - the `"minecart_composite"` key is now `"composite"`
        - the `"minecart"` key is now `"base"`
    - The old recipe types with their old keys are still recognized; this will be removed in the first version that
    supports Minecraft 1.22
    - Added new builtin data packs:
      - Uncraft Chest Boats; default: `enabled`
      > Adds recipes to separate Chest Boats into a Chest and a Boat.  
      The Chest is the recipe output while the Boat is left behind in the crafting grid and receives the name of the
      input
      Chest Boat.
      - Keep Chest Boat Names; default: `enabled`
      > Replace vanilla's Chest Boat recipes with new recipes that transfer the name of the input Boat to the recipe's
      output.
      - Fragile Old Carts; default: `disabled`
      > Make Minecarts break apart into 3 Iron Ingots. Minecarts with 'Thing's will also drop the 'Thing'.
      Similar to pre-1.3.1 boat behavior.  
      *Not intended to be enabled alongside "Fragile Carts".*
      - Fragile Chest Boats; default: `disabled`
      > Makes Chest Boats break apart into a Chest and a Boat instead of dropping themselves.  
      *Not intended to be enabled alongside "Fragile Old Boats".*
      - Fragile Old Boats; default: `disabled`
      > Makes Boats break apart into 2 Sticks and 3 Planks (and a Chest in the case of Chest Boats) instead of dropping
      themselves, similar to pre-1.3.1 behavior.  
      *Not intended to be enabled alongside \"Fragile Chest Boats\".*
    - All builtin data packs now have translatable names and descriptions
    - All data is now generated
    - All recipes now have advancements to unlock them (the datagen made me do it)
- 1.3.0 (17 Jun. 2024):
  - Updated for 1.21
  - Updated translations
  - Minor internal changes
- 1.2.0 (21 May 2024):
  - Updated for 1.20.5 and 1.20.6
  - From this version onward, to connect to a server with Sturdy Carts,
**either** [Polymer](https://modrinth.com/mod/polymer/versions) must be installed on the *server*,
**or** Sturdy Carts must be installed on the *client*.  
This is due to changes to the way recipes work in 1.20.5.
  - Improved [Mod Menu](https://modrinth.com/mod/modmenu) integration
- 1.1.2 (19 Jan. 2024): Updated for 1.20.3-4
- 1.1.1 (19 Jan. 2024): Updated for 1.20.2
- 1.1.0 (14 Aug. 2023): Replaced Nbt Crafting-dependant recipes with custom recipes, closes [#1](https://gitlab.com/supersaiyansubtlety-group/minecraft-mods/sturdy_carts/-/issues/1)
- 1.0.6 (23 Jun. 2023): Updated for 1.20 and 1.20.1!
- 1.0.5 (4 Apr. 2023): Updated for 1.19.3 and 1.19.4
- 1.0.4 (7 Aug. 2022): Marked as compatible with 1.19.2
- 1.0.3 (29 Jul. 2022):
  - Marked as compatible with 1.19.1.
  - Minor internal changes.
- 1.0.2 (27 Jun. 2022):
  - Updated for 1.19!
  - Since Minecart with <Thing>s drop themselves by default in 1.19, I've replaced the old loot tables with ones that restore the pre-1.19 behavior of dropping a Minecart and a \<Thing\>. They're disabled by default.
- 1.0.1 (16 Jun. 2022): Marked as compatible with 1.18.2
- 1.0.0 (18 Jan. 2022): Initial release version!
- 0.0.1 (16 Jan. 2022): Initial version.