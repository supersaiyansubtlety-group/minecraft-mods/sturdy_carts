<!--required for Modrinth centering -->
<center>
<div style="text-align:center;line-height:250%">

## Sturdy Vehicles

[![Minecraft versions](https://cf.way2muchnoise.eu/versions/Minecraft_sturdy-vehicles_all.svg)](https://modrinth.com/mod/sturdy-vehicles/versions#all-versions)
![environment: server](https://img.shields.io/badge/environment-server-orangered)  
[![loader: Fabric](https://img.shields.io/badge/loader-Fabric-cdc4ae?logo=data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMjYgMjgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoyIj48cGF0aCBkPSJtMTAgMjQtOC04TDE0IDRWMmgybDYgNnY0TDEwIDI0eiIgc3R5bGU9ImZpbGw6I2RiZDBiNCIvPjxwYXRoIGQ9Ik0xMiA2djJoLTJ2Mkg4djJINnYySDR2MkgydjRoMnYyaDJ2MmgydjJoMnYtMmgydi0yaDJ2LTJoMnYtMmgydi0yaDJ2LTRoLTJ2LTJoLTJWOGgtMlY2aDJ2MmgydjJoMnYyaDJ2Mmgydi0yaC0yVjhoLTJWNmgtMlY0aC0yVjJoMnYyaDJ2MmgydjJoMnYyaDJ2NGgtMnYyaC00djJoLTJ2MmgtMnYyaC0ydjRoLTJ2Mkg4di0ySDZ2LTJINHYtMkgydi0ySDB2LTRoMnYtMmgydi0yaDJ2LTJoMlY4aDJWNmgyVjJoMlYwaDJ2MmgtMnY0aC0yIiBzdHlsZT0iZmlsbDojMzgzNDJhIi8+PHBhdGggc3R5bGU9ImZpbGw6IzgwN2E2ZCIgZD0iTTIyIDEyaDJ2MmgtMnoiLz48cGF0aCBkPSJNMiAxOGgydjJoMnYyaDJ2MmgydjJIOHYtMkg2di0ySDR2LTJIMnYtMiIgc3R5bGU9ImZpbGw6IzlhOTI3ZSIvPjxwYXRoIGQ9Ik0yIDE2aDJ2MmgydjJoMnYyaDJ2Mkg4di0ySDZ2LTJINHYtMkgydi0yeiIgc3R5bGU9ImZpbGw6I2FlYTY5NCIvPjxwYXRoIGQ9Ik0yMiAxMnYtMmgtMlY4aC0yVjZsLTIuMDIzLjAyM0wxNiA4aDJ2MmgydjJoMnpNMTAgMjR2LTJoMnYtNGg0di00aDJ2LTJoMnY0aC0ydjJoLTJ2MmgtMnYyaC0ydjJoLTIiIHN0eWxlPSJmaWxsOiNiY2IyOWMiLz48cGF0aCBkPSJNMTQgMThoLTR2LTJIOHYtMmgydjJoNHYyem00LTRoLTR2LTJoLTJ2LTJoLTJWOGgydjJoMnYyaDR2MnpNMTQgNGgydjJoLTJWNHoiIHN0eWxlPSJmaWxsOiNjNmJjYTUiLz48L3N2Zz4=)](https://fabricmc.net/)
<a href="https://quiltmc.org/"><img width=73 src="https://raw.githubusercontent.com/QuiltMC/art/master/brand/1024png/quilt_available_dark.png" alt="available for: Quilt Loader"></a>  
<a href="https://modrinth.com/mod/fabric-api/versions"><img width="60" src="https://i.imgur.com/Ol1Tcf8.png" alt="Requires: Fabric API"></a>
[![supports: Polymer](https://img.shields.io/badge/supports-Polymer-4fd3ff?logo=data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj48c3ZnIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIHZpZXdCb3g9IjAgMCAxMCAxMCIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxuczpzZXJpZj0iaHR0cDovL3d3dy5zZXJpZi5jb20vIiBzdHlsZT0iZmlsbC1ydWxlOmV2ZW5vZGQ7Y2xpcC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjI7Ij48cGF0aCBpZD0iaGFuZGxlLWxpZ2h0IiBzZXJpZjppZD0iaGFuZGxlIGxpZ2h0IiBkPSJNMCw5bDAsLTAuNzVsMC43NSwwbDAsLTAuNzVsMC43NSwwbDAsLTAuNzVsMC43NSwwbDAsLTAuNzVsMC43NSwwbDAsLTAuNzVsMC43NSwwbDAsLTAuNzVsMC43NSwwbDAsLTAuNzVsMC43NSwwbDAsLTAuNzVsMC43NSwwbDAsLTAuNzVsMC43NSwwbDAsMS41bC0wLjc1LDBsMCwwLjc1bC0wLjc1LDBsMCwwLjc1bC0wLjc1LDBsMCwwLjc1bC0wLjc1LDBsMCwwLjc1bC0wLjc1LDBsMCwwLjc1bC0wLjc1LDBsMCwwLjc1bC0wLjc1LDBsMCwwLjc1bC0xLjUsMFptNy41LC02Ljc1bDAsLTEuNWwxLjUsMGwwLDAuNzVsLTAuNzUsMGwwLDAuNzVsLTAuNzUsMFoiIHN0eWxlPSJmaWxsOiM4OTY3Mjc7Ii8+PHBhdGggaWQ9ImhhbmRsZS1kYXJrIiBzZXJpZjppZD0iaGFuZGxlIGRhcmsiIGQ9Ik0wLDkuNzVsMS41LDBsMCwtMC43NWwwLjc1LDBsMCwtMC43NWwwLjc1LDBsMCwtMC43NWwwLjc1LDBsMCwtMC43NWwwLjc1LDBsMCwtMC43NWwwLjc1LDBsMCwtMC43NWwwLjc1LDBsMCwtMC43NWwwLjc1LDBsMCwtMC43NWwwLjc1LDBsMCwtMC43NWwtMC43NSwwbDAsMC43NWwtMC43NSwwbDAsMC43NWwtMC43NSwwbDAsMC43NWwtMC43NSwwbDAsMC43NWwtMC43NSwwbDAsMC43NWwtMC43NSwwbDAsMC43NWwtMC43NSwwbDAsMC43NWwtMC43NSwwbDAsMC43NWwtMS41LDBsMCwwLjc1Wm04LjI1LC03LjVsMCwtMC43NWwwLjc1LDBsMCwwLjc1bC0wLjc1LDBaIiBzdHlsZT0iZmlsbDojNDkzNjE1OyIvPjxwYXRoIGlkPSJoZWFkLWRhcmstZ3JheSIgc2VyaWY6aWQ9ImhlYWQgZGFyayBncmF5IiBkPSJNNiwyLjI1bC0zLDBsMCwtMC43NWwtMC43NSwwbDAsLTAuNzVsMC43NSwwbDAsLTAuNzVsMy43NSwwbDAsMC43NWwwLjc1LDBsMCwwLjc1bC0wLjc1LDBsMCwtMC43NWwtMy43NSwwbDAsMC43NWwzLDBsMCwwLjc1Wm0yLjI1LDBsMC43NSwwbDAsMC43NWwtMC43NSwwbDAsLTAuNzVaIiBzdHlsZT0iZmlsbDojNTg1ODU4OyIvPjxwYXRoIGlkPSJoZWFkLWxpZ2h0LWdyYXkiIHNlcmlmOmlkPSJoZWFkIGxpZ2h0IGdyYXkiIGQ9Ik05LDMuNzVsLTAuNzUsMGwwLC0wLjc1bC0xLjUsMGwwLC0wLjc1bC0wLjc1LDBsMCwtMC43NWwtMywwbDAsLTAuNzVsMy43NSwwbDAsMS41bDEuNSwwbDAsMC43NWwwLjc1LDBsMCwwLjc1WiIgc3R5bGU9ImZpbGw6I2Q4ZDhkODsiLz48cGF0aCBpZD0iaGVhZC1saWdodC1ibHVlIiBzZXJpZjppZD0iaGVhZCBsaWdodCBibHVlIiBkPSJNOSw2Ljc1bC0wLjc1LDBsMCwtM2wtMC43NSwwbDAsLTAuNzVsMC43NSwwbDAsMC43NWwwLjc1LDBsMCwzWm0tMS41LC00LjVsLTAuNzUsMGwwLC0wLjc1bDAuNzUsMGwwLDAuNzVaIiBzdHlsZT0iZmlsbDojNTBkMmZlOyIvPjxwYXRoIGlkPSJoZWFkLWRhcmstYmx1ZSIgc2VyaWY6aWQ9ImhlYWQgZGFyayBibHVlIiBkPSJNOSwzbDAuNzUsMGwwLDMuNzVsLTAuNzUsMGwwLDAuNzVsLTAuNzUsMGwwLC0wLjc1bC0wLjc1LDBsMCwtM2wwLjc1LDBsMCwzbDAuNzUsMGwwLC0zLjc1WiIgc3R5bGU9ImZpbGw6IzBmNzI5NDsiLz48L3N2Zz4=)](https://modrinth.com/mod/polymer/versions)
[![supports: Mod Menu](https://img.shields.io/badge/supports-Mod_Menu-134bfe?logo=data:image/webp+xml;base64,UklGRlIAAABXRUJQVlA4TEUAAAAv/8F/AA9wpv9T/M///McDFLeNpKT/pg8WDv6jiej/BAz7v+bbAKDn9D9l4Es/T/9TBr708/Q/ZeBLP0//c7Xiqp7z/QEA)](https://modrinth.com/mod/modmenu/versions)  
[![license: MIT](https://img.shields.io/badge/license-MIT-white)](https://gitlab.com/supersaiyansubtlety-group/minecraft-mods/sturdy_vehicles/-/blob/master/LICENSE)
[![source on: GitLab](https://img.shields.io/badge/source_on-GitLab-fc6e26?logo=gitlab)](https://gitlab.com/supersaiyansubtlety-group/minecraft-mods/sturdy_vehicles)
[![issues: GitLab](https://img.shields.io/gitlab/issues/open-raw/supersaiyansubtlety-group/minecraft-mods/sturdy_vehicles?label=issues&logo=gitlab)](https://gitlab.com/supersaiyansubtlety-group/minecraft-mods/sturdy_vehicles/-/issues)
[![localized: Percentage](https://badges.crowdin.net/sturdy-vehicles/localized.svg)](https://crwd.in/sturdy-vehicles)  
[![Modrinth: Downloads](https://img.shields.io/modrinth/dt/sturdy-vehicles?logo=modrinth&label=Modrinth&color=00ae5d)](https://modrinth.com/mod/sturdy-vehicles/versions)
[![CurseForge: Downloads](https://img.shields.io/curseforge/dt/570040?logo=curseforge&label=CurseForge&color=f16437)](https://www.curseforge.com/minecraft/mc-mods/sturdy-vehicles/files)  
[![chat: Discord](https://img.shields.io/discord/1006391289006280746?logo=discord&color=5964f3)](https://discord.gg/xABmPngXAH)
<a href="https://coindrop.to/supersaiyansubtlety"><img width="82" style="border-radius:3px" src="https://coindrop.to/embed-button.png" alt="coindrop.to me"></a>
<a href="https://ko-fi.com/supersaiyansubtlety"><img width="109" src="https://i.ibb.co/4gwRR8L/p.png" alt="Buy me a coffee"></a>

</div>
</center>

---

### Improve Minecart and Boat recipes, control their drops! Data driven.

Formerly known as Sturdy Carts.

Works server-side* and in single player.  
*see Prior Versions below for Minecraft versions 1.20.5-1.21.1.

Sturdy Vehicles adds recipes to separate a Minecart or Boat item from its held item.  
e.g. if you put a Minecart with Chest in a crafting grid you can pull the Hopper out from the result slot and the
Minecart will remain in the grid with the Chest removed.  
You can do the same with Furnace, Hopper, and TNT Minecarts and with Chest Boats.

A renamed Minecart or Boat will keep its name when split from or combined with its held item.

Sturdy Vehicles also allows changing of Minecart and Boat drops, but doesn't change any by default.  
Some of the builtin packs change Loot Tables to emulate older versions of Minecraft.

Recipes and Loot Tables are data driven, and everything included is in builtin packs that can be
enabled|disabled using `/datapack enable|disable "sturdy_vehicles:pack_name"`.

Mods and mod packs can add similar recipes, read how on
[the wiki](https://gitlab.com/supersaiyansubtlety-group/minecraft-mods/sturdy_vehicles/-/wikis).

---

<details>

<summary>Builtin packs</summary>

- Uncraft Minecarts; default: `enabled`
> Adds recipes to separate Minecart with 'Thing's into a Minecart and a 'Thing'.  
The 'Thing' is the recipe output while the Minecart is left behind in the crafting grid and receives the name of the
input Minecart with 'Thing'.

- Uncraft Chest Boats; default: `enabled`
> Adds recipes to separate Chest Boats into a Chest and a Boat.  
The Chest is the recipe output while the Boat is left behind in the crafting grid and receives the name of the input
Chest Boat.

- Keep Minecart Names; default: `enabled`
> Replace vanilla's Minecart with 'Thing' recipes with new recipes that transfer the name of the input Minecart to the
recipe's output.

- Keep Chest Boat Names; default: `enabled`
> Replace vanilla's Chest Boat recipes with new recipes that transfer the name of the input Boat to the recipe's output.

- Fragile Carts; default: `disabled`
> Make Minecart with 'Thing's break apart into a Minecart and a 'Thing' instead of dropping themselves.
Restores pre-1.19 behavior.  
*Not intended to be enabled alongside "Fragile Old Carts".*

- Fragile Old Carts; default: `disabled`
> Make Minecarts break apart into 3 Iron Ingots. Minecarts with 'Thing's will also drop the 'Thing'.
Similar to pre-1.3.1 boat behavior.  
*Not intended to be enabled alongside "Fragile Carts".*

- Fragile Chest Boats; default: `disabled`
> Makes Chest Boats break apart into a Chest and a Boat instead of dropping themselves.  
*Not intended to be enabled alongside "Fragile Old Boats".*

- Fragile Old Boats; default: `disabled`
> Makes Boats break apart into 2 Sticks and 3 Planks (and a Chest in the case of Chest Boats) instead of dropping
themselves, similar to pre-1.3.1 behavior.  
*Not intended to be enabled alongside \"Fragile Chest Boats\".*

</details>

---

<details>

<summary>Prior versions</summary>

Mod versions prior to 2.0.0 are named Sturdy Carts, its id is `sturdy_carts`,
and it adds the recipe types `sturdy_carts:minecart_crafting` and `sturdy_carts:minecart_uncrafting`.

On Minecraft versions 1.20.5-1.21.1, to connect to a server with Sturdy Vehicles,
**either** [Polymer](https://modrinth.com/mod/polymer/versions) must be installed on the *server*,
**or** Sturdy Vehicles must be installed on the *client*.

Mod versions prior to 1.1.0 only add recipes if [Nbt Crafting](https://modrinth.com/mod/nbt-crafting) is installed.

On Minecraft versions prior to 1.19, the mod makes Minecarts with 'Thing's drop themselves
(instead of splitting into a Minecart and a 'Thing' which is the pre-1.19 vanilla behavior).

</details>

---

<details>

<summary>Translating</summary>

[![localized: Percentage](https://badges.crowdin.net/sturdy-vehicles/localized.svg)](https://crwd.in/sturdy-vehicles)  
If you'd like to help translate Sturdy Vehicles, you can do so on
[Crowdin](https://crwd.in/sturdy-vehicles).

</details>

---

<details>

<summary>Credits</summary>

- [gliscowo](https://github.com/gliscowo) for making
[Isometric Renders](https://modrinth.com/mod/isometric-renders/versions), which I used to create the mod icon.

</details>

---

This mod is only for Fabric (works on Quilt, too!) and I won't be porting it to Forge. The license is MIT, however, so
anyone else is free to port it.

I'd appreciate links back to this page if you port or otherwise modify this project, but links aren't required.
